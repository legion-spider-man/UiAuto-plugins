import base64

from pyDes import des, ECB, PAD_PKCS5


def desencryption(params):
    # 加密密钥
    des_secret_key = params['des_secret_key']
    # 偏移量
    iv_value = params['iv_value'] if params['iv_value'] else des_secret_key
    des_obj = des(des_secret_key, ECB, iv_value, pad=None, padmode=PAD_PKCS5)  # 初始化一个des对象，参数是秘钥，加密方式，偏移， 填充方式
    key_secret = base64.b64encode(des_obj.encrypt(params['pwd'].encode()))
    return key_secret


ad=desencryption({"des_secret_key": "testasds", "pwd": "ssss", "iv_value": ""})
print(ad)
