"""
by suyj
"""
from pykeyboard import PyKeyboard
import time
import sys
import json
import base64
import uiautomation as uiauto
from selenium.common.exceptions import NoSuchElementException
from browser import ResumeBrowser, generate_xpath
import traceback
import hard_keyboard

keyboard = PyKeyboard()


def press_down():
    keyboard.tap_key(keyboard.down_key)


def press_up():
    keyboard.tap_key(keyboard.up_key)


def press_tab():
    keyboard.tap_key(keyboard.tab_key)


def send_keys(str_, interval=.2):
    keyboard.type_string(str_, interval)


def press_esc():
    keyboard.tap_key(keyboard.escape_key)


def press_left():
    keyboard.tap_key(keyboard.left_key)


def press_right():
    keyboard.tap_key(keyboard.right_key)


def backspace():
    keyboard.tap_key(keyboard.backspace_key)


def press_enter():
    keyboard.tap_key(keyboard.enter_key)


def press_space():
    keyboard.tap_key(keyboard.space_key)


def user_input(params):
    operations = params['operation']
    operate_times = int(params.get('operate_times', 1))
    interval = float(params.get('interval'))
    if operations == 'send_keys':
        input_text = params['input_text']
        for _ in range(operate_times):
            eval('send_keys(input_text, interval)')
            time.sleep(interval)
    else:
        for _ in range(operate_times):
            eval('{}()'.format(operations))
            time.sleep(interval)
    return


def key_send(params):
    try:

        element = None
        xpath = None
        if params['element_type'] == 'Xpath':
            if 'browser_info' in params.keys() and params['browser_info'] is not None:
                driver = params['browser_info']
                element = driver.find_element_by_xpath(params['element_xpath'])
                driver.execute_script("arguments[0].scrollIntoView()", element)
                if element is not None:
                    element.clear()
                    element.send_keys(params['content'])
            else:
                raise Exception("缺少参数：浏览器对象")
        elif params['element_type'] == 'Browser':
            if params['browser_info'] is None or params['browser_info'] == '':
                raise Exception("缺少参数：浏览器对象")
            if params['target_browser_element'] is None or params['target_browser_element'] == '':
                raise Exception("缺少参数：选择目标")

            target_element = params['target_browser_element']
            driver = params['browser_info']
            html = target_element['html']

            try:
                element = driver.find_element_by_xpath(html['xpath'])
            except NoSuchElementException:
                element = (driver.find_element_by_xpath(
                    html['full_xpath']) if 'full_xpath' in html.keys() else None)

            driver.execute_script("arguments[0].scrollIntoView()", element)
            if element is not None:
                element.clear()
                element.send_keys(params['content'])
            else:
                raise Exception('元素不存在')
        elif params['element_type'] == 'Native':
            if params['target_native_element'] is None or params['target_native_element'] == '' or 'wnd' not in params[
                'target_native_element'].keys():
                raise Exception("缺少参数：选择目标")

            wnd = params['target_native_element']['wnd']
            if "control_type_name" not in wnd.keys() or wnd["control_type_name"] is None or wnd[
                "control_type_name"] == "":
                raise Exception(message="客户端界面元素没有control_type_name")
            elif "name" not in wnd.keys() or wnd["name"] is None or wnd["name"] == "":
                raise Exception(message="客户端界面元素没有name属性")
            elif hasattr(uiauto, wnd["control_type_name"]) is False:
                raise Exception(message="uiautomation不支持目标元素类型")
            else:
                control = getattr(uiauto, wnd["control_type_name"])(
                    Name=wnd['name'])
                if hasattr(control, "SendKeys"):
                    control.SendKeys(params['content'])
        else:
            raise Exception(message="尚未支持当前元素的操作")

        return None
    except Exception as e:
        raise e


def Key_combination(params):
    try:
        # print(keyboard.numpad_keysas)
        command = params['command']
        letter = params['letter'].replace(' ', '').strip().lower() if params['letter'] else ""
        # print('letter>>>>>>>>>>>>', letter)
        if letter == 'esc':
            letter = keyboard.escape_key
        elif letter == 'pause':
            letter = keyboard.pause_key
        elif letter == 'backspace':
            letter = keyboard.backspace_key
        elif letter == 'delete':
            letter = keyboard.delete_key
        elif letter == 'insert':
            letter = keyboard.insert_key
        elif letter == 'tab':
            letter = keyboard.tab_key
        elif letter == 'enter':
            letter = keyboard.enter_key
        elif letter == 'capslock':
            letter = keyboard.caps_lock_key
        elif letter == 'numlock':
            letter = keyboard.num_lock_key
        elif letter == 'scrolllock':
            letter = keyboard.scroll_lock_key
        elif letter == 'space':
            letter = keyboard.space_key
        elif letter == 'pageup':
            letter = keyboard.page_up_key
        elif letter == 'pagedown':
            letter = keyboard.page_down_key
        elif letter == 'home':
            letter = keyboard.home_key
        elif letter == 'up':
            letter = keyboard.up_key
        elif letter == 'down':
            letter = keyboard.down_key
        elif letter == 'left':
            letter = keyboard.left_key
        elif letter == 'right':
            letter = keyboard.right_key
        elif letter == 'print':
            letter = keyboard.print_key
        elif letter == 'snapshot' or letter == 'printscreen':
            letter = keyboard.snapshot_key
        elif letter == 'help':
            letter = keyboard.help_key
        elif letter == 'end':
            letter = keyboard.end_key
        elif letter == 'f1':
            letter = keyboard.function_keys[1]
        elif letter == 'f2':
            letter = keyboard.function_keys[2]
        elif letter == 'f3':
            letter = keyboard.function_keys[3]
        elif letter == 'f4':
            letter = keyboard.function_keys[4]
        elif letter == 'f5':
            letter = keyboard.function_keys[5]
        elif letter == 'f6':
            letter = keyboard.function_keys[6]
        elif letter == 'f7':
            letter = keyboard.function_keys[7]
        elif letter == 'f8':
            letter = keyboard.function_keys[8]
        elif letter == 'f9':
            letter = keyboard.function_keys[9]
        elif letter == 'f10':
            letter = keyboard.function_keys[10]
        elif letter == 'f11':
            letter = keyboard.function_keys[11]
        elif letter == 'f12':
            letter = keyboard.function_keys[12]
        elif letter == '+':
            letter = keyboard.numpad_keys['Add']
        elif letter == '*':
            letter = keyboard.numpad_keys['Multiply']
        elif letter == '/':
            letter = keyboard.numpad_keys['Divide']
        elif letter == '-':
            letter = keyboard.numpad_keys['Subtract']
        else:
            letter = params['letter'].split("+")
        if command:
            if isinstance(letter, list):
                eval('{}(letter)'.format(command))
            else:
                eval('{}([letter])'.format(command))

    except Exception as e:
        raise e


def ctrl(letter):
    keyboard.press_keys([keyboard.control_key] + letter)


def windows(letter):
    keyboard.press_keys([keyboard.windows_l_key] + letter)


def shift(letter):
    keyboard.press_keys([keyboard.shift_key] + letter)


def alt(letter):
    keyboard.press_keys([keyboard.alt_key] + letter)


def altshift(letter):
    keyboard.press_keys([keyboard.alt_key, keyboard.shift_key] + letter)


def ctrlalt(letter):
    keyboard.press_keys([keyboard.control_key, keyboard.alt_key] + letter)


def ctrlshift(letter):
    keyboard.press_keys([keyboard.control_key, keyboard.shift_key] + letter)


def windowshift(letter):
    keyboard.press_keys([keyboard.windows_l_key, keyboard.shift_key] + letter)


def windowsalt(letter):
    keyboard.press_keys([keyboard.windows_l_key, keyboard.alt_key] + letter)


def windowsctrl(letter):
    keyboard.press_keys([keyboard.windows_l_key, keyboard.control_key] + letter)


def ctrlaltshift(letter):
    keyboard.press_keys([keyboard.control_key, keyboard.alt_key, keyboard.shift_key] + letter)


def hard_keyboard_input(params):
    hard_keyboard.user_input(params)


if __name__ == '__main__':
    # keyboard.press_keys([keyboard.windows_r_key,'l'])
    import time
    time.sleep(3)
    Key_combination({
        "letter": "c",
        "command": "ctrl"
    })

