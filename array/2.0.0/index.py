def createArray(params):
    try:
        return []
    except Exception as e:
        raise e


def arrayadd(params):
    try:
        array = params['array']
        ele = params['ele']
        if ',' in ele:
            list_split = ele.split(',')
            for i in list_split:
                array.append(i)
        else:
            array.append(ele)
        return array
    except Exception as e:
        raise e

def arrayremove(params):
    try:
        array = params['array']
        ele = params['ele']
        if ',' in ele:
            list_split = ele.split(',')
            for i in list_split:
                array.remove(i)
        else:
            array.remove(ele)
        return array
    except Exception as e:
        raise e

def filterArray(params):
    try:
        array = params['array']
        ele = params["ele"]
        isSave = params["isSave"]
        aList = list()
        if ele in array:
            if isSave == 'yes':
                return array
            else:
                for a in array:
                    if a != ele:
                        aList.append(a)
                return aList
        return array
    except Exception as e:
        raise e


def toString(params):
    try:
        array = params['array']
        s = str(params["s"])
        array = map(str, array)
        string = s.join(array)
        return string
    except Exception as e:
        raise e


def getIndex(params):
    try:
        array = params['array']
        # s_list = array.split(",")
        if len(array) == 0:
            data = 0
        else:
            data = len(array)
        return data
    except Exception as e:
        raise e

