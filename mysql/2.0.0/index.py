import pymysql

def connectMysql(params):
    """
    连接数据库
    """
    try:
        if 'host' not in params or 'port' not in params or 'username' not in params or 'password' not in params or 'database_name' not in params:
            raise Exception('缺少参数')
        host = params.get('host')
        port = params.get('port', 3306)  # 端口这里默认是3306
        try:
            port = int(port)
        except Exception:
            return "端口信息错误"
        username = params.get('username')
        password = params.get('password')
        database_name = params.get('database_name')
        encoding = params.get('encoding')
        conn = pymysql.Connect(host=host, port=port, user=username, password=password, database=database_name,
                               charset=encoding)
        return conn
    except pymysql.err.OperationalError:
        return "数据库连接超时"
    except Exception as e:
        raise e


def executeSelectSQL(params):
    """
    执行查询SQL语句
    """
    conn = params.get('connect_message')
    sql = params.get('sql')
    try:
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        if sql.startswith("select") or sql.startswith("SELECT"):
            cursor.execute(sql)
            result = cursor.fetchall()
            return result
        else:
            return {}
    except Exception as e:
        raise e


def executeOtherSQL(params):
    """
    执行其他SQL语句
    """
    conn = params.get('connect_message')
    sql = params.get('sql')
    try:
        cursor = conn.cursor()
        row_affect = cursor.execute(sql)
        conn.commit()
        return row_affect
    except pymysql.err.IntegrityError:
        return 0
    except pymysql.err.InternalError:
        return 0
    except Exception as e:
        raise e


def closeConnect(params):
    """
    关闭数据库
    """
    try:
        conn = params.get('connect_message')
        conn.close()
        return True
    except IndexError as e:
        return f"请检查：{e.__str__()}"
    except Exception:
        return False

if __name__ == '__main__':
    pass
