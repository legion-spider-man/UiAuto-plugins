import json
import math
import re
from time import localtime, time, strptime, mktime
from datetime import datetime, timedelta
import demjson
import win32con
import win32clipboard as wincld
import pandas as pd


def createDataTable(params):
    try:
        th = params['th']
        array = params['array']

        data = pd.DataFrame(array, columns=th)
        # data.fillna(value=None)
        # for col in th:
        #     clean_z = data[col].fillna(0)
        #     clean_z[clean_z==0.0] = "null"
        #     data[col] = clean_z
        string = data.to_dict()
        print(string)
        return string
    except Exception as e:
        raise e


def toDataFrame(string):
    from io import StringIO
    TESTDATA = StringIO(string)
    df = pd.read_csv(TESTDATA, sep=";")
    return df


def toPaste(params):
    try:
        data = params["dt"]
        # df = toDataFrame(data)
        df = pd.DataFrame(data)
        df.to_clipboard()
        # set_text(data)
        return df
    except Exception as e:
        raise e


def dataFilter(params):
    try:
        condition = params['filter']
        colName = params['colName']
        value = params["c"]
        dt = params["dt"]

        df = pd.DataFrame(dt)
        # datatable = p['result']

        query = None
        if condition == 'bigger':
            if isinstance(value, int) or isinstance(value, float):
                query = colName + ">" + str(value)
            else:
                query = colName + ">\'" + str(value) + "\'"

        if condition == "smaller":
            # query = colName + "<" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + "<" + str(value)
            else:
                query = colName + "<\'" + str(value) + "\'"
        if condition == "bigequal":
            # query = colName + ">=" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + ">=" + str(value)
            else:
                query = colName + ">=\'" + str(value) + "\'"
        if condition == "smallequal":
            # query = colName + "<=" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + "<=" + str(value)
            else:
                query = colName + "<=\'" + str(value) + "\'"
        if condition == "equal":
            # query = colName + "==" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + "==" + str(value)
            else:
                query = colName + "==\'" + str(value) + "\'"
        if condition == "noequal":
            # query = colName + "!=" + value
            if isinstance(value, int) or isinstance(value, float):
                query = colName + "!=" + str(value)
            else:
                query = colName + "!=\'" + str(value) + "\'"
        if condition == "none":
            query = colName + "==" + "\'\'"
        if condition == "notnone":
            query = colName + "!=" + "\'\'"
        if condition == "start":
            query = colName + ".str.startswith(\'" + value + "\')"
        if condition == "end":
            query = colName + ".str.endswith(\'" + value + "\')"
        if condition == "nostart":
            query = "~" + colName + ".str.startswith(\'" + value + "\')"
        if condition == "noend":
            query = "~" + colName + ".str.endswith(\'" + value + "\')"
        if condition == "contain":
            query = colName + ".str.contains(\'" + value + "\')"
        if condition == "nocontain":
            query = "~" + colName + ".str.contains(\'" + value + "\')"
        print("条件：", query)
        result = df.query(query)
        return result
    except Exception as e:
        raise e


def dataCut(params):
    try:
        
        row = params['row']

        col = params['col']
        dt = params['dt']
        # dt = re.sub(r"\s{2,}", " ", dt)
        # print(dt)
        # df = toDataFrame(dt)
        df = pd.DataFrame(dt)

        length = len(row)
        if length == 0:
            result = df.loc[:, col]
        else:
            result = df.loc[row[0]:row[1], col]

        return result.to_string()

    except Exception as e:
        raise e


def dataSelect(params):
    try:

        col = params['col']
        data = params['dt']
        df = pd.DataFrame(data)
        array = df.filter(items=col)

        return array
    except Exception as e:
        raise e


def dataRepeat(params):
    try:

        col = params['col']
        data = params['dt']
        df = pd.DataFrame(data)
        keep = params['keep']
        if col == []:
            col = None
        result = df.drop_duplicates(subset=col, keep=keep)
        return result

    except Exception as e:
        raise e


def toArray(params):
    try:
        data = params['dt']
        hasCol = params['hasCol']
        df = pd.DataFrame(data)
        result = df.values.tolist()
        if hasCol == 'yes':
            cols = df.columns.tolist()
            result.insert(0, cols)

        return result
    except Exception as e:
        raise e


def tableCompare(params):
    try:
        data1 = params['dt']
        data2 = params['dt1']
        df = pd.DataFrame(data1)
        df1 = pd.DataFrame(data2)
        return df.equals(df1)
        # th1 = p1['col']

    except Exception as e:
        raise e


def addCol(params):
    try:
        data = params['dt']
        colName = params['colName']
        value = params['data']
        position = params['position']
        df = pd.DataFrame(data)
        if position == 'null':
            df[colName] = value
        else:
            position = int(position)
            df.insert(position, colName, value)
        return df
    except KeyError as e:
        raise e
    except Exception as e:
        raise e


def changeType(params):
    try:
        data = params['dt']
        col = params['col']
        dataType = params['type']
        # isExcept = params['except']
        value = params['data']
        df = pd.DataFrame(data)


        if type(col) == list:
            for c in col:
                df[c] = df[c].apply(convert, args=(dataType, value))
        else:
            df[col] = df[col].apply(convert, args=(dataType, value))
        return df
    except Exception as e:
        raise e


def convert(a, dataType, value):
    try:
        if dataType == 'int':
            return int(a)
        if dataType == 'float':
            return float(a)
        if dataType == 'str':
            return str(a)
    except ValueError as e:
        try:
            # if isExcept:
            #     raise e
            # else:
            return value
        finally:
            del e


def dataSort(params):
    try:
        sort = params['sort']
        data = params['dt']
        col = params['col']
        df = pd.DataFrame(data)
        if sort == 'yes':
            sort = True
        else:
            sort = False
        result = df.sort_values(by=col, ascending=sort)
        return result
    except Exception as e:
        raise e


def getColNum(params):
    try:
        data = params['dt']
        df = pd.DataFrame(data)
        return df.shape

    except Exception as e:
        raise e


def getColName(params):
    try:
        data = params['dt']
        df = pd.DataFrame(data)
        return df.columns.tolist()
    except Exception as e:
        raise e


def updateColName(params):
    try:
        data = params['dt']
        col = params['col']
        df = pd.DataFrame(data)
        df.columns = col
        return df

    except Exception as e:
        raise e


def tableMerge(params):
    try:
        sort = params['sort']
        data = params['dt']
        data1 = params['dt1']
        connect = params['connect']
        leftCol = params['leftCol']
        rightCol = params['rightCol']
        if sort == 'no':
            sort = False
        else:
            sort = True
        df = pd.DataFrame(data)
        df1 = pd.DataFrame(data1)
        result = pd.merge(df, df1, how=connect, left_on=leftCol,
                          right_on=rightCol, sort=sort)
        return result

    except Exception as e:
        raise e


def replaceStr(params):
    try:
        string = str(params['string'])
        target = str(params['target'])
        replace = str(params['repl'])
        ignore = params['ignore']
        newStr = string.replace(target, replace)
        if ignore == 'no':
            reg = re.compile(re.escape(target), re.IGNORECASE)
            newStr = reg.sub(replace, string)

        return newStr
    except Exception as e:
        raise e


def findStr(params):
    try:
        string = str(params['string'])
        target = str(params['target'])
        ignore = str(params["ignore"])

        position = int(params["pos"])
        if position == 0:
            raise Exception("请输入1或比1大的整数")
        if position >= 1:
            position = position - 1
        index = string.find(target, position)
        if ignore == 'no':
            tarList = re.findall(target, string, flags=re.IGNORECASE)
            if len(tarList) > 0:
                index = string.find(tarList[0], position)
            else:
                index = -1
        if index == -1:
            return 0

        return index + 1
    except Exception as e:
        raise e


def reFindStr(params):
    try:
        string = str(params['string'])
        target = str(params['target'])
        ignore = str(params["ignore"])
        try:
            position = int(params["pos"])
            if position == 0:
                return "请输入1或比1大的整数"
            if position >= 1:
                position = position - 1
        except ValueError:
            return "请输入数字"
        index = string.rfind(target, position)
        if ignore == 'no':
            tarList = re.findall(target, string, flags=re.IGNORECASE)
            index = string.find(tarList[len(tarList) - 1], position)
        if index == -1:
            return "没找到"

        return index + 1
    except Exception as e:
        raise e


def leftSub(params):
    try:
        string = str(params["string"])
        num = int(params['num'])
        result = string[:num]
        return result
    except Exception as e:
        raise e


def rightSub(params):
    try:
        string = str(params["string"])
        num = int(params['num'])
        result = string[-num:]
        return result
    except Exception as e:
        raise e


def middleSub(params):
    try:
        string = str(params["string"])
        num = int(params["num"]) - 1
        length = int(params["length"])
        result = string[num:length + num]
        return result
    except Exception as e:
        raise e


def getLength(params):
    try:
        string = params["string"]
        return len(string)
    except Exception as e:
        raise e


def getByteLength(params):
    try:
        string = str(params["string"])
        return len(string.encode())
    except Exception as e:
        raise e


def toUpper(params):
    try:
        string = params["string"]
        return string.upper()
    except Exception as e:
        raise e


def toLower(params):
    try:
        string = params["string"]
        return string.lower()
    except Exception as e:
        raise e


def getAscii(params):
    try:
        a = params["char"][0]
        return ord(a)
    except Exception as e:
        raise e


def toChar(params):
    try:
        code = int(params['code'])
        return chr(code)
    except Exception as e:
        raise e


def leftCut(params):
    try:
        string = str(params["string"])
        cut = str(params['cut'])
        while string.startswith(cut):
            string = string.lstrip(cut)
        return string
    except Exception as e:
        raise e


def rightCut(params):
    try:
        string = str(params["string"])
        cut = str(params['cut'])
        while string.endswith(cut):
            string = string.rstrip(cut)
        return string
    except Exception as e:
        raise e


def allCut(params):
    try:
        string = str(params["string"])
        cut = str(params['cut'])
        if string.startswith(cut):
            string = leftCut(params)
            params['string'] = string
        if string.endswith(cut):
            string = rightCut(params)
        return string
    except Exception as e:
        raise e


def getLenStr(params):
    try:
        string = str(params["string"])
        num = int(params["num"]) - 1
        length = int(params["length"])
        result = string[num:length + num]
        return result
    except Exception as e:
        raise e


def getPosStr(params):
    try:
        string = str(params["string"])
        num = int(params["start"]) - 1
        length = int(params["end"])
        result = string[num:length + num - 1]
        return result
    except Exception as e:
        raise e


def middleCut(params):
    try:
        string = str(params["string"])
        start = int(params['start'])
        length = int(params["length"])

        result = string[:start - 1] + string[length + start - 1:]

        return result
    except Exception as e:
        raise e


def createBlank(params):
    try:
        num = int(params["num"])
        return '"%s"' % (" " * num)
    except ValueError:
        raise Exception("请输入整数")
    except Exception as e:
        raise e


def createStr(params):
    try:
        num = int(params["num"])
        string = str(params["string"])
        return num * string
    except Exception as e:
        raise e


def splitStr(params):
    try:
        if str(params['s']) != "":
            s = str(params["s"])
        else:
            s = " "
        string = str(params["string"])
        slist = string.split(s)
        slist = list(filter(not_empty, slist))
        # result = print_result(slist)
        return slist
    except Exception as e:
        raise e


def compareStr(params):
    try:
        case = str(params["case"])
        string1 = str(params['string1'])
        string2 = str(params['string2'])
        if case == 'yes':
            reg = re.compile(re.escape(string1), re.IGNORECASE)
        else:
            reg = re.compile(re.escape(string1))
        if reg.match(string2):
            result = "相同"
        else:
            result = "不相同"
        return result
    except Exception as e:
        raise e


def compareLenStr(params):
    try:
        case = str(params["case"])
        string1 = str(params['string1'])
        string2 = str(params['string2'])
        length = int(params["length"])
        if length > 0:
            string1 = string1[:length]
            string2 = string2[:length]
        else:
            return "请输入正数"
        if case == 'yes':
            reg = re.compile(re.escape(string1), re.IGNORECASE)
        else:
            reg = re.compile(re.escape(string1))
        if reg.match(string2):
            result = "相同"
        else:
            result = "不相同"
        return result
    except Exception as e:
        raise e


def getChar(params):
    try:
        string = str(params["string"])
        position = int(params['pos'])
        result = string[position - 1:position]
        return result
    except Exception as e:
        raise e


def not_empty(s):
    return s and s.strip() and s is not None and s != ''


def reverseStr(params):
    try:
        string = params["string"]
        slist = list(string)
        slist.reverse()
        result = "".join(slist)
        return result
    except Exception as e:
        raise e


if __name__ == '__main__':
    df1 = pd.DataFrame({'key': ['X', 'Y', 'Z'], 'data_set_1': [1, 2, 3]})
    df2 = pd.DataFrame({'key': ['X', 'B', 'C'], 'data_set_2': [4, 5, 6]})
    params = {}
    params['sort'] = 'no'
    params['dt'] = df1
    params['dt1'] = df2
    params['connect'] = 'outer'
    params['leftCol'] = ['key', 'data_set_1']
    params['rightCol'] = ['key', 'data_set_2']
    print(tableMerge(params))