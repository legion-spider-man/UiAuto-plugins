#coding=utf-8

from turtle import st
import uuid
import os
import time
import re
import copy
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def puppeteerFn(str, params):
    file_name = '{}.html'.format(uuid.uuid1())
    father_path = os.path.dirname(__file__)
    file_name = os.path.join(father_path, file_name)

    fh = open(file_name, 'w')
    fh.write(str)
    fh.close()


    chromeOptions = webdriver.ChromeOptions()
    chromeOptions.add_argument('--no-sandbox')
    # chromeOptions.add_argument('headless')
    executable_path = params['uiauto_config']["client_dir"] + \
                "/env/webdriver/" + sys.platform + "/chromedriver.exe"
    if sys.platform == "darwin":
        executable_path = params['uiauto_config']["client_dir"] + \
                "/env/webdriver/" + sys.platform + "/chromedriver"
    driver = webdriver.Chrome(executable_path, options=chromeOptions) # 调用带参数的谷歌浏览器

    driver.minimize_window()
    driver.get("file://" + file_name)

    if params['is_copy']:
        if sys.platform == "win32":
            driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL,'a')
            driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL,'c')

        if sys.platform == "darwin":
            driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND,'a')
            driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND,'c')

    time.sleep(1)
    driver.quit()
    

def text_template(params):

    # 模板
    str = params['template']
    str = str.replace('\n', '')
    list = re.findall(r"&lt;for span (.+?)&lt;/for&gt;", str)
    table_list = re.findall(r"<p>&lt;for table (.+?)&lt;/for&gt;</p>", str)
    image1 = '<img width="734" height="583" src="' + params['template_value']['image1'] + '">'
    image2 = '<img width="734" height="583" src="' + params['template_value']['image2'] + '">'

    if len(list):
        print("list")
        # 整理格式
        target = []
        for list_item in list:
            target.append({"key": list_item.split('&gt;')[0], "old_value": list_item.split('&gt;')[1], "new_value": ""})

        for target_item in target:
            temp = ''
            for data_item in params['template_value'][target_item['key'].replace("{", "").replace("}", "")]:
                copy_target_item = copy.deepcopy(target_item['old_value'])
                temp += copy_target_item.format(**data_item)
            target_item['new_value'] = temp
            # 遍历结束，替换模板值
            str = str.replace("&lt;for span " + target_item['key'] + "&gt;" + target_item['old_value'] + "&lt;/for&gt;", target_item['new_value'])

    if len(table_list):
        print("table_list")
        
        # 整理格式
        target = []
        for table_list_item in table_list:
            target.append({"key": table_list_item.split('&gt;')[0], "old_value": table_list_item.split('&gt;')[1], "new_value": ""})
        
        # 整理需要替换的表格行
        for target_item in target:
            result = ''
            target_tr = ''
            temp_tr = re.findall(r"<tr (.+?)</tr>", target_item['old_value'])
            temp_tr1 = temp_tr[1]
            # print(temp_tr)
            for data_item in params['template_value'][target_item['key'].replace("{", "").replace("}", "")]:
                copy_target_item = copy.deepcopy(temp_tr1)
                target_tr += "<tr " + copy_target_item.format(**data_item) + "</tr>"
            target_item['new_value'] = target_tr
            copy_old_value = copy.deepcopy(target_item['old_value'])
            copy_old_value = copy_old_value.replace(re.findall(r"<tr (.+?)</tr>", copy_old_value)[1], target_item['new_value'])
            # 遍历结束，替换模板值
            result += copy_old_value
            str = str.replace("<p>&lt;for table " + target_item['key'] + "&gt;" + target_item['old_value'] + "&lt;/for&gt;</p>", result)
    
    res = str.format(date=params['template_value']['date'],image1=image1,image2=image2)
    puppeteerFn(res, params)
    return res

if __name__ == "__main__":
    res = text_template({
        "template": "<p><span style='font-size: 16.0pt; font-family: 黑体;'>每日疫情情况通报</span></p><p><span style='font-size: 14.0pt; font-family: 黑体;'>{date}</span></p><p class='MsoListParagraph' style='margin-left: 36.0pt; text-indent: -36.0pt; mso-char-indent-count: 0; mso-list: l0 level1 lfo2;'><strong style='mso-bidi-font-weight: normal;'><span lang='EN-US' style='font-size: 14.0pt; font-family: 华文楷体; mso-bidi-font-family: 华文楷体;'><span style='mso-list: Ignore;'>1、</span></span></strong><strong style='mso-bidi-font-weight: normal;'><span style='font-size: 14.0pt; font-family: 华文楷体;'>新增情况</span></strong></p><p><span style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>&lt;for span {data}&gt;{area}新增</span><span lang='EN-US' style='font-size: 14.0pt; font-family: 华文楷体; color: red;'>{new_local}</span><span style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>例，&lt;/for&gt;具体如下表，全国新增情况见附件。</span></p><p class='MsoListParagraph' style='margin-left: 36.0pt; text-indent: -36.0pt; mso-char-indent-count: 0; mso-list: l0 level1 lfo2;'><strong style='mso-bidi-font-weight: normal;'><span lang='EN-US' style='font-size: 14.0pt; font-family: 华文楷体; mso-bidi-font-family: 华文楷体;'><span style='mso-list: Ignore;'>2、</span></span></strong><strong style='mso-bidi-font-weight: normal;'><span style='font-size: 14.0pt; font-family: 华文楷体;'>汇总信息</span></strong></p><p>&lt;for table {data}&gt;</p><table style='border-collapse: collapse; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 0cm 0cm 0cm;' border='0' cellspacing='0' cellpadding='0'><tbody><tr style='mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 8.5pt;'><td style='width: 61.6pt; border: solid windowtext 1.0pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='82'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left;' align='left'><strong> <span style='font-size: 14.0pt; font-family: 黑体; mso-font-kerning: 1.0pt;'>地区</span> </strong></p></td><td style='width: 72.7pt; border: solid windowtext 1.0pt; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='97'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left;' align='left'><strong><span style='font-size: 14.0pt; font-family: 黑体; mso-font-kerning: 1.0pt;'>新增本土</span></strong></p></td><td style='width: 70.85pt; border: solid windowtext 1.0pt; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='94'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left;' align='left'><strong><span style='font-size: 14.0pt; font-family: 黑体; mso-font-kerning: 1.0pt;'>现有病例</span></strong></p></td><td style='width: 70.9pt; border: solid windowtext 1.0pt; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='95'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left;' align='left'><strong><span style='font-size: 14.0pt; font-family: 黑体; mso-font-kerning: 1.0pt;'>累计确诊</span></strong></p></td><td style='width: 70.85pt; border: solid windowtext 1.0pt; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='94'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left;' align='left'><strong><span style='font-size: 14.0pt; font-family: 黑体; mso-font-kerning: 1.0pt;'>境外输入</span></strong></p></td><td style='width: 70.9pt; border: solid windowtext 1.0pt; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='95'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left;' align='left'><strong><span style='font-size: 14.0pt; font-family: 黑体; mso-font-kerning: 1.0pt;'>累计治愈</span></strong></p></td><td style='width: 2.0cm; border: solid windowtext 1.0pt; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='76'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left;' align='left'><strong><span style='font-size: 14.0pt; font-family: 黑体; mso-font-kerning: 1.0pt;'>治愈率</span></strong></p></td></tr><tr style='height: 8.5pt;'><td style='width: 61.6pt; border: solid windowtext 1.0pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='82'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left;' align='left'><span style='font-size: 14.0pt; font-family: 黑体; mso-font-kerning: 1.0pt;'>{area}</span></p></td><td style='width: 72.7pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='97'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: right;' align='right'><span lang='EN-US' style='font-size: 14.0pt; font-family: 黑体; color: red; mso-font-kerning: 1.0pt;'>&nbsp;{new_local}</span></p></td><td style='width: 70.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='94'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: right;' align='right'><span lang='EN-US' style='font-size: 14.0pt; font-family: 黑体; color: red; mso-font-kerning: 1.0pt;'>&nbsp;{exist}</span></p></td><td style='width: 70.9pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5p' valign='top' width='95'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: right;' align='right'><span lang='EN-US' style='font-size: 14.0pt; font-family: 黑体; color: red; mso-font-kerning: 1.0pt;'>&nbsp;{accumulation}</span></p></td><td style='width: 70.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt' valign='top' width='94'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: right;' align='right'><span lang='EN-US' style='font-size: 14.0pt; font-family: 黑体; color: red; mso-font-kerning: 1.0pt;'>&nbsp;{import}</span></p></td><td style='width: 70.9pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='95'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: right;' align='right'><span lang='EN-US' style='font-size: 14.0pt; font-family: 黑体; color: #70ad47; mso-font-kerning: 1.0pt;'>&nbsp;{cure}</span></p></td><td style='width: 2.0cm; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; background: yellow; padding: 0cm 5.4pt 0cm 5.4pt; height: 8.5pt;' valign='top' width='76'><p style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: center;' align='center'><span lang='EN-US' style='font-size: 14.0pt; font-family: 黑体; color: #70ad47; mso-font-kerning: 1.0pt;'>{cure_rate}</span></p></td></tr></tbody></table><p>&lt;/for&gt;</p><p class='MsoNormal' style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; margin-left: 36.0pt; text-align: left; text-indent: -36.0pt;' align='left'><strong><span lang='EN-US' style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>3、</span></strong><span lang='EN-US' style='font-size: 13.5pt; font-family: 微软雅黑,sans-serif; color: black;'>&nbsp;</span><strong><span style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>国内新增与境外输入趋势</span></strong></p><p class='MsoNormal' style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; text-indent: 21.0pt;' align='left'><span lang='EN-US' style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>A．</span><span style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>国内新增趋势</span></p><p class='MsoNormal' style='text-indent: 21pt;' align='left'><span style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>{image1}</span></p><p class='MsoNormal' style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; text-indent: 21.0pt;' align='left'><span lang='EN-US' style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>B．</span><span style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>新增境外输入趋势</span></p><p class='MsoNormal' style='mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; text-align: left; text-indent: 21.0pt;' align='left'><span style='font-size: 14.0pt; font-family: 华文楷体; color: black;'>{image2}</span></p>",
        "template_value": {
            "data": [{"area": "江苏", "new_local": "1", "exist": "2", "accumulation": "3", "import": "4", "cure": "5", "cure_rate": "6%"}, {"area": "深圳", "new_local": "1", "exist": "2", "accumulation": "3", "import": "4", "cure": "5", "cure_rate": "6%"},{"area": "广东", "new_local": "7", "exist": "8", "accumulation": "9", "import": "10", "cure": "11", "cure_rate": "12%"}, {"area": "香港", "new_local": "1", "exist": "2", "accumulation": "3", "import": "4", "cure": "5", "cure_rate": "6%"}],
            "date": "2022年03月15日 星期一",
            "image1": "image1",
            "image2": "image2"
        }
    })
    print(res)