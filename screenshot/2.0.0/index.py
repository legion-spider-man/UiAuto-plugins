import os
from PIL import ImageGrab
import sys
import time


def Screenshot(params):
    """
    截全屏
    """
    if params['path']:
        try:
            path = os.path.join(params['path'], 'UIAuto_photo' + time.strftime("%F"))
            # path = params['path'] + '/UIAuto_photo/' + time.strftime("%F")
            if not os.path.exists(path):
                os.makedirs(path)
            save_path = os.path.join(path, 'UIAuto_' + time.strftime("%F_%H_%M_%S") + '.png')
        except:
            return 'wrong path'
    else:
        return 'There is no path to save'
    try:
        px = ImageGrab.grab()
        width, high = px.size  # 获得当前屏幕的大小
        bbox = (0, 0, width, high)
        im = ImageGrab.grab(bbox)
        im.save(save_path)
        return save_path
    except Exception as e:
        return 'Screenshot failed to save\n{}'.format(e)


def ScreenshotArea(params):
    """
    区域截屏
    """
    if params['path']:
        try:
            path = os.path.join(params['path'], 'UIAuto_photo' + time.strftime("%F"))
            # path = params['path'] + '/UIAuto_photo/' + time.strftime("%F")
            if not os.path.exists(path):
                os.makedirs(path)
            save_path = os.path.join(path, 'UIAuto_' + time.strftime("%F_%H_%M_%S") + '.png')
        except:
            return 'wrong path'
    else:
        return 'There is no path to save'
    if params['imobj_path']:
        with open(save_path, 'wb') as fp:
            img_file = open(params['imobj_path'], mode='rb')
            fp.write(img_file.read())
            img_file.close()
            pass
        return save_path
    else:
        return 'wrong path'

def main(params):
    try:
        verify_code = Screenshot(params)
    except:
        verify_code = 'Screenshot() ERROR'
    console_result(verify_code)
    return verify_code


def console_result(verify_code):
    sys.stdout.write('<uiauto:result>{}</uiauto:result>'.format(verify_code))

if __name__ == '__main__':
    params = {
        'path': r'E:\note',
        'imobj_path': r'C:\UiAuto_files\projects\main\screenshot\2022-05-07\2022-05-07_15_32_23.png'
    }
    result = ScreenshotArea(params)
    print(result)