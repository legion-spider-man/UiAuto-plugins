import re


def find(params):
    try:
        # global pDict
        ##目标字符串
        string = params['string']
        ##正则表达式(匹配的是括号里的内容)
        regex = params['regex']
        # p = params['p']
        # 正则表达式配置
        flags = 0
        U, S, I, L, M, X = 0, 0, 0, 0, 0, 0
        if params['p'] != '':
            plist = params['p']
            if ',' in plist:
                plist = plist.split(',')
                for i in plist:
                    if i == 'U':
                        U = re.U
                    if i == 'S':
                        S = re.S
                    if i == 'I':
                        I = re.I
                    if i == 'L':
                        L = re.L
                    if i == 'M':
                        M = re.M
                    if i == 'X':
                        X = re.X
                flags = (U | X | M | L | I | S)
        else:
            flags = 0

        objPattern = re.compile(regex, flags)
        objMatcher = re.search(objPattern, string)
        print(objMatcher)
        if objMatcher is None:
            return []
        else:
            arrRet = []
            arrRet.append(objMatcher.group(0))
            for s in objMatcher.groups():
                arrRet.append(s)
            return arrRet
        # return pDict
    except Exception as e:
        raise e


def findChild(params):
    try:
        # global pDict
        string = params['string']
        regex = params['regex']
        num = int(params["num"])
        flags = 0
        U, S, I, L, M, X = 0, 0, 0, 0, 0, 0
        if params['p'] != '' and params['p']:
            plist = params['p']
            if ',' in plist:
                plist = plist.split(',')
                for i in plist:
                    if i == 'U':
                        U = re.U
                    if i == 'S':
                        S = re.S
                    if i == 'I':
                        I = re.I
                    if i == 'L':
                        L = re.L
                    if i == 'M':
                        M = re.M
                    if i == 'X':
                        X = re.X
                flags = (U | X | M | L | I | S)
        else:
            flags = 0

        print(flags)
        print(params['p'])

        objPattern = re.compile(regex, flags)
        objMatcher = re.search(objPattern, string)
        if objMatcher is None:
            return ""
        else:
            return objMatcher.group(num)


    except Exception as e:
        raise e


def findAll(params):
    try:
        # global pDict
        string = params['string']
        regex = params['regex']
        flags = 0
        U, S, I, L, M, X = 0, 0, 0, 0, 0, 0
        if params['p'] != '' and params['p']:
            plist = params['p']
            if ',' in plist:
                plist = plist.split(',')
                for i in plist:
                    if i == 'U':
                        U = re.U
                    if i == 'S':
                        U = re.S
                    if i == 'I':
                        U = re.I
                    if i == 'L':
                        U = re.L
                    if i == 'M':
                        U = re.M
                    if i == 'X':
                        U = re.X
                flags = (U | X | M | L | I | S)
        else:
            flags = 0

        pattern = re.compile(regex, flags)
        it = pattern.findall(string)
        # result = '['
        # if len(it) > 0:

        #     return it
        # else:
        #     return []
        return it
    except Exception as e:
        raise e
