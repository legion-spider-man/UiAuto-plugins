
def create(params):
    try:

        objSet = set()
        return objSet
    except Exception as e:
        raise e


def add(params):
    try:
        # global objSet
        if isinstance(params['set'], list):
            s = set(params['set'])
        else:
            s = params['set']
        ele = params['ele']
        if ',' in ele:
            list_split=ele.split(',')
            for i in list_split:
                s.add(i)
        else:
            s.add(ele)
        return s
    except Exception as e:
        raise e


def remove(params):
    try:
        # global objSet
        if isinstance(params['set'], list):
            s = set(params['set'])
        else:
            s = set(params['set'])
        ele = params['ele']
        if ',' in ele:
            list_split = ele.split(',')
            for i in list_split:
                s.remove(i)
        else:
            s.remove(ele)
        return s
    except Exception as e:
        raise e


def get_size(params):
    try:
        # global objSet
        if isinstance(params['set'], list):
            objSet = set(params['set'])
        else:
            raise Exception("集合格式不正确")

        # s_set = set(s_list)
        length = len(objSet)
        return length
    except Exception as e:
        raise e


def getIntersection(params):
    try:
        # s = params["s"].replace("{", "").replace("}", "").replace("'", "")
        # s1 = params["s1"].replace("{", "").replace("}", "").replace("'", "")
        if params['s']and params['s1']:
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("")

        intersection = s.intersection(s1)
        if len(intersection) == 0:
            return "没有交集"
        return set(intersection)
    except Exception as e:
        raise e


def hasIntersection(params):
    try:
        if params['s']and params['s1']:
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("请输入两个集合")

        intersection = s.intersection(s1)
        if len(intersection) == 0:
            return "没有交集"
        else:
            return set(intersection)
    except Exception as e:
        raise e


def isSubset(params):
    try:

        if params['s']and params['s1']:
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("请输入两个集合")
        if s.issubset(s1) is False:
            return "不是子集"
        else:
            return "是子集"
    except Exception as e:
        raise e


def isSuperset(params):
    try:

        if params['s']and params['s1']:
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("请输入两个集合")
        if s.issuperset(s1) is False:
            return "不是父集"
        else:
            return "是父集"
    except Exception as e:
        raise e


def getDiffer(params):
    try:
        if params['s']and params['s1']:
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("请输入两个集合")
        differ = s - s1
        if len(differ) == 0:
            return "没有差集"
        return list(differ)
        # result = ''
    except Exception as e:
        raise e


def union(params):
    try:

        if params['s']and params['s1']:
            s = set(params['s'])
            s1 = set(params['s1'])
        else:
            raise Exception("请输入两个集合")
        u_set = s.union(s1)
        if len(u_set) == 0:
            return "没有并集"
        return list(u_set)
        # result = ''

    except Exception as e:
        raise e



