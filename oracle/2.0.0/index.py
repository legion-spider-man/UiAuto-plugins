from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import datetime
import os
import shutil
from traceback import print_exc
import decimal

"""
need:cx-Oracle==6.0, 32bit oracle11g-client(oci.dll, oraocci11.dll, oraociei11.dll), 32bit python.exe
"""
# 防止中文打印乱码
os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'


def connectOracle(params):
    """
    连接数据库
    """
    try:
        current_dir = os.path.split(os.path.realpath(__file__))[0]
        if os.path.exists(params['uiauto_config']['client_dir'] + '\\oci.dll') is False:
            shutil.copy(current_dir + '\\oci.dll', params['uiauto_config']['client_dir'] + '\\oci.dll')

        if os.path.exists(params['uiauto_config']['client_dir'] + '\\oraocci11.dll') is False:
            shutil.copy(current_dir + '\\oraocci11.dll', params['uiauto_config']['client_dir'] + '\\oraocci11.dll')

        if os.path.exists(params['uiauto_config']['client_dir'] + '\\oraociei11.dll') is False:
            shutil.copy(current_dir + '\\oraociei11.dll', params['uiauto_config']['client_dir'] + '\\oraociei11.dll')
        if 'username' not in params or 'password' not in params or 'host' not in params or 'database_name' not in params:
            raise Exception('请检查参数')
        connect_info = "oracle://{}:{}@{}/{}?utf8".format(params['username'], params['password'], params['host'],
                                                          params['database_name'])
        engine = create_engine(connect_info, echo=False)  # mysql连接语句
        db_session = sessionmaker(bind=engine)
        session = db_session()
        return session
    except Exception as e:
        raise e



def executeSelectSQL(params):
    """
    执行查询sql语句
    """
    try:
        session = params['session']
        res_list = []
        res = session.execute(params['sql']).fetchall()
        for one in res:
            one_row = []
            for clu in one:
                if type(clu) is datetime.datetime:
                    clu = str(clu)
                elif type(clu) is decimal.Decimal:
                    clu = str(clu)
                one_row.append(clu)
            res_list.append(one_row)
        # pprint(json.dumps(res_list, ensure_ascii=False))
        # session.close()
        return res_list
    except Exception as ex:
        print_exc()
        raise ex


def executeOtherSQL(params):
    """
    执行其他sql语句
    """
    try:
        session = params['session']
        result = session.execute(params['sql'])
        session.commit()
        return result.rowcount
    except Exception as ex:
        print_exc()
        raise ex


def closeOracle(params):
    """
    关闭数据库
    """
    try:
        session = params['session']
        session.close()
    except Exception as ex:
        print_exc()
        raise ex


if __name__ == "__main__":
    params1 = {'username': 'system', 'password': '123456', 'host': '192.168.0.175:1521', 'database': 'orcl'}
    db = connectOracle(params=params1)
    params2 = {
        'session': db,
        'sql': "insert into scott.dept(deptno, dname, loc) values(1, 'Jason', '123123')"
    }
    executeOtherSQL(params=params2)
