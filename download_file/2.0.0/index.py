import requests


def saveFileFromUrl(params):
    try:
        if 'download_url' not in params.keys() or params['download_url'] is None or params['download_url'] == '':
            raise Exception('缺少参数：图片下载链接')
        elif 'save_path' not in params.keys() or params['save_path'] is None or params['save_path'] == '':
            raise Exception('缺少参数：保存目录')
        elif 'file_name' not in params.keys() or params['file_name'] is None or params['file_name'] == '':
            raise Exception('缺少参数：保存名称')
        else:
            file_path = "%s/%s" % (params['save_path'], params['file_name'])
            if params['request_method'] == 'post':
                res = requests.get(params['download_url'], data=params['request_params'],
                                   headers=params['request_headers'])
            elif params['request_method'] == 'get':
                res = requests.get(params['download_url'], data=params['request_params'],
                                   headers=params['request_headers'])
            else:
                res = None

            if res is not None:
                with open(file_path, "wb") as save_file:
                    for chunk in res.iter_content(chunk_size=512):
                        if chunk:
                            save_file.write(chunk)
            else:
                raise Exception("请求失败")

        return file_path
    except Exception as e:
        raise e


if __name__ == "__main__":
    # saveImageFromUrl({'download_url':'https://img0.baidu.com/it/u=2862534777,914942650&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500', 'image_path':'E://hwx', 'image_name':'huangkjsfkd.png'} )
    pass