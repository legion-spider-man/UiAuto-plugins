import re
import json
from selenium import webdriver
from selenium.webdriver.chrome import options
from selenium.common.exceptions import InvalidArgumentException
from browser import ResumeBrowser, generate_xpath
import time
import sys
from selenium.webdriver.common.action_chains import ActionChains
# sys.path.insert(0, "C:\\UiAuto\\public\\pyscript\\base\\")
import base64
import os
from selenium.common.exceptions import NoSuchElementException
import platform


def goto_url(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        # driver = ResumeBrowser(
        #     browser_info['executor_url'], browser_info['session_id'])
        driver.get(params['url'])
        return None
    else:
        raise Exception('没有启动浏览器')


def click_ele(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        mouse_key = params['mouse_key']
        click_type = params['click_type']

        if params['element_type'] == 'Xpath':
            element = driver.find_element_by_xpath(params['element_xpath'])
            driver.execute_script("arguments[0].scrollIntoView()", element)
            if element is not None:
                if mouse_key == 'left':
                    if click_type == 'simple':
                        element.click()
                    elif click_type == 'double':
                        ActionChains(driver).double_click(element).perform()
                    else:
                        raise Exception("尚未支持该点击类型")
                elif mouse_key == 'right':
                    if click_type == 'simple':
                        ActionChains(driver).context_click(element).perform()
                    elif click_type == 'double':
                        ActionChains(driver).context_click(element).perform()
                        time.sleep(.1)
                        ActionChains(driver).context_click(element).perform()
                    else:
                        raise Exception("尚未支持该点击类型")
                else:
                    raise Exception("尚未支持该鼠标键的点击操作")
        elif params['element_type'] == 'Browser':
            if params['browser_info'] is None or params['browser_info'] == '':
                raise Exception("缺少参数：浏览器对象")
            if params['target_browser_element'] is None or params['target_browser_element'] == '':
                raise Exception("缺少参数：选择目标")

            driver = params['browser_info']
            driver.switch_to.default_content()

            target_element = params['target_browser_element']
            html = target_element['html']

            if html['frame'] is not None:
                switch_frame(driver, html['frame'])

            try:
                element = driver.find_element_by_xpath(html['xpath'])
            except NoSuchElementException:
                element = (driver.find_element_by_xpath(html['full_xpath']) if 'full_xpath' in html.keys() else None)

            driver.execute_script("arguments[0].scrollIntoView()", element)

            if element is not None:
                if mouse_key == 'left':
                    if click_type == 'simple':
                        element.click()
                    elif click_type == 'double':
                        ActionChains(driver).double_click(element).perform()
                    else:
                        raise Exception("尚未支持该鼠标操作类型")
                elif mouse_key == 'right':
                    if click_type == 'simple':
                        ActionChains(driver).context_click(element).perform()
                    elif click_type == 'double':
                        ActionChains(driver).context_click(element).perform()
                        time.sleep(.1)
                        ActionChains(driver).context_click(element).perform()
                    else:
                        raise Exception("尚未支持该鼠标操作类型")
                else:
                    raise Exception("尚未支持该鼠标键的操作")
            else:
                raise Exception("无法定位元素")
    else:
        raise Exception(message="缺少参数：浏览器对象")


def switch_frame(driver, iframe):
    try:
        iframe_el = driver.find_element_by_xpath(iframe['full_xpath'])
        if iframe_el is not None:
            driver.switch_to.frame(iframe_el)
            if iframe['next_frame'] is not None:
                switch_frame(driver, iframe['next_frame'])
                # swich_frame(driver, iframe['next_frame'])
    except Exception as e:
        driver.switch_to.default_content()


def openBrowser(params):
    try:
        executable_path = ""
        global driver
        if params['browser_type'] == "Internet Explorer":
            executable_path = params['uiauto_config']["client_dir"] + \
                              "/env/webdriver/" + sys.platform + "/IEDriverServer.exe"
            driver = webdriver.Ie(executable_path=executable_path)
        elif params['browser_type'] == "Microsoft Edge":
            from msedge.selenium_tools import Edge
            from msedge.selenium_tools import EdgeOptions
            executable_path = params['uiauto_config']["client_dir"] + \
                              "/env/webdriver/" + sys.platform + "/msedgedriver.exe"
            # driver = webdriver.Edge(executable_path=executable_path)
            edgeOptions = EdgeOptions()
            # edgeOptions.use_chromium = True
            edgeOptions.add_argument('--no-sandbox')
            edgeOptions.add_experimental_option('excludeSwitches', ['enable-automation'])
            if params['is_headless']:
                # edgeOptions.headless(True)
                edgeOptions.use_chromium = True
                edgeOptions.add_argument('--headless')
                edgeOptions.add_argument('--disable-gpu')
            driver = Edge(executable_path=executable_path, options=edgeOptions)
            if params['is_stealth']:
                edgeOptions.use_chromium = True
                root_path = os.path.join(os.path.dirname(__file__), 'stealth.min.js')
                if not os.path.exists(root_path):
                    raise Exception(f'文件不存在: {root_path}')
                with open(root_path, 'r') as f:
                    js = f.read()
                driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
                    "source": js
                })
            else:
                driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
                    "source": """
                        Object.defineProperty(navigator, 'webdriver', {
                          get: () => undefined
                        })
                      """
                })
        elif params['browser_type'] == "Chrome":
            chromeOptions = webdriver.ChromeOptions()
            chromeOptions.add_argument('--no-sandbox')
            chromeOptions.add_experimental_option('excludeSwitches', ['enable-automation'])
            if params['is_headless']:
                chromeOptions.add_argument('--headless')
                chromeOptions.add_argument('--disable-gpu')
            executable_path = params['uiauto_config']["client_dir"] + \
                              "/env/webdriver/" + sys.platform + "/chromedriver.exe"
            if sys.platform.startswith("darwin") or sys.platform.startswith("linux"):
                executable_path = params['uiauto_config']["client_dir"] + \
                              "/env/webdriver/" + sys.platform + "/chromedriver"
            driver = webdriver.Chrome(executable_path=executable_path, options=chromeOptions)
            if params['is_stealth']:
                root_path = os.path.join(os.path.dirname(__file__), 'stealth.min.js')
                if not os.path.exists(root_path):
                    raise Exception(f'文件不存在: {root_path}')
                with open(root_path, 'r') as f:
                    js = f.read()
                driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
                    "source": js
                })
            else:
                driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
                    "source": """
                        Object.defineProperty(navigator, 'webdriver', {
                          get: () => undefined
                        })
                      """
                })
        else:
            raise Exception("暂未支持该浏览器")
        driver.maximize_window()
        driver.get(params['url'])
        return driver
    except Exception as e:
        raise e


def getPage(params):
    # 重连浏览器
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        # browser_info = params['browser_info']
        driver = params['browser_info']
        return driver.page_source
    else:
        raise Exception('没有启动浏览器')


def injectJs(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        # driver = ResumeBrowser(
        #     browser_info['executor_url'], browser_info['session_id'])
        inject_result = driver.execute_script(params["js_code"])
        return inject_result
    else:
        raise Exception('没有启动浏览器')


def closeBrowser(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        # driver = ResumeBrowser(
        #     browser_info['executor_url'], browser_info['session_id'])
        # driver.close()
        driver.quit()
        return None
    else:
        raise Exception('没有启动浏览器')


def getCookies(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        # driver = ResumeBrowser(
        #     browser_info['executor_url'], browser_info['session_id'])
        cookies = driver.get_cookies()
        return ';'.join([c['name'] + '=' + c['value'] for c in cookies])
    else:
        raise Exception('没有启动浏览器')


def remoteBrowser(params):
    try:
        if 'executor_url' in params.keys() and "session_id" in params.keys():
            driver = ResumeBrowser(
                params['executor_url'], params['session_id'])
            # executor_url = driver.command_executor._url
            # session_id = driver.session_id

            return driver
        else:
            raise Exception(message="缺少参数！")
    except Exception as e:
        raise e


def resolvingListData(params):
    try:
        result = {
            "headers": [],
            "items": []
        }
        if 'browser_info' in params.keys() and params['browser_info'] is not None \
                and 'css_selector' in params.keys() and params['css_selector'] is not None:
            driver = params["browser_info"]
            css_selector = params["css_selector"]
            # if 'executor_url' in browser_info.keys() and "session_id" in browser_info.keys():
            # driver = ResumeBrowser(
            #     browser_info['executor_url'], browser_info['session_id'])

            headers = []
            items = []
            element = driver.find_element_by_css_selector(css_selector)
            thead = element.find_element_by_tag_name("thead")

            ths = element.find_elements_by_tag_name("th")
            ignore_index = []
            column_index = []
            if ths is not None and len(ths) > 0:
                for i, th in enumerate(ths):
                    colspan = th.get_attribute("colspan")
                    if colspan:
                        start = column_index[len(
                            column_index) - 1] if len(column_index) > 0 else 0
                        for j in range(start + 1, start + int(colspan) + 1):
                            column_index.append(j)
                            ignore_index.append(
                                j) if th.text in params["ignore_column"] else None
                    else:
                        column_index.append(i)
                        ignore_index.append(i) if th.text in params["ignore_column"] else headers.append(
                            th.text.replace("↓", ""))
            result['headers'] = headers + params["add_column"]

            tbody = element.find_element_by_tag_name("tbody")
            trs = tbody.find_elements_by_tag_name("tr")
            if trs and len(trs) > 0:
                for tr in trs:
                    temp_item = []
                    tds = tr.find_elements_by_tag_name("td")
                    if tds and len(tds) > 0:
                        for i, td in enumerate(tds):
                            temp_item.append(
                                td.text) if i not in ignore_index else None
                        for ac in params["add_column"]:
                            temp_item.append("")
                        items.append(temp_item)
            result["items"] = items

            return result
        else:
            raise Exception(message="缺少参数！")
    except Exception as e:
        raise e


"""
TODO:
- 切换浏览器标签页
"""


def switchBrowserTag(params):
    try:
        target_handle = None
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'match_target' not in params.keys():
            raise Exception("缺少参数：匹配对象")
        elif 'match_content' not in params.keys():
            raise Exception("缺少参数：匹配内容")
        elif 'match_type_select' not in params.keys():
            raise Exception("缺少参数：切换方式")
        elif 'match_type' not in params.keys():
            raise Exception("缺少参数：匹配方式")
        elif 'handle' not in params.keys():
            raise Exception("缺少参数：标签页句柄")
        else:
            driver = params['browser_info']

            if params['match_type_select'] == "match":
                window_handles = driver.window_handles
                if len(window_handles) > 0:
                    for handle in window_handles:
                        driver.switch_to_window(handle)
                        print("current_url:" + driver.current_url, driver.current_url == params['match_content'])
                        if params['match_type'] == "equal":
                            if target_handle is None and params['match_target'] == "Title" and driver.title == params['match_content']:
                                target_handle = handle
                                break
                            if target_handle is None and params['match_target'] == "Url" and driver.current_url == params['match_content']:
                                target_handle = handle
                                break
                        elif params['match_type'] == "contains":
                            if target_handle is None and params['match_target'] == "Title" and params['match_content'] in driver.title:
                                target_handle = handle
                                break
                            if target_handle is None and params['match_target'] == "Url" and params['match_content'] in driver.current_url:
                                target_handle = handle
                                break
                else:
                    raise Exception('浏览器没有标签页')
            elif params['match_type_select'] == "handle":
                target_handle = params['handle']

        if target_handle is None:
            raise Exception('没有找匹配的标签页')
        else:
            driver.switch_to_window(target_handle)
        return target_handle
    except Exception as e:
        raise e


def closeBrowserTag(params):
    try:
        target_handle = None
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'match_target' not in params.keys():
            raise Exception("缺少参数：匹配对象")
        elif 'match_content' not in params.keys():
            raise Exception("缺少参数：匹配内容")
        elif 'match_type_select' not in params.keys():
            raise Exception("缺少参数：切换方式")
        elif 'match_type' not in params.keys():
            raise Exception("缺少参数：匹配方式")
        elif 'handle' not in params.keys():
            raise Exception("缺少参数：标签页句柄")
        else:
            driver = params['browser_info']

            if params['match_type_select'] == "match":
                window_handles = driver.window_handles
                if len(window_handles) > 0:
                    for handle in window_handles:
                        driver.switch_to_window(handle)
                        print("current_url:" + driver.current_url, driver.current_url == params['match_content'])
                        if params['match_type'] == "equal":
                            if target_handle is None and params['match_target'] == "Title" and driver.title == params['match_content']:
                                target_handle = handle
                                break
                            if target_handle is None and params['match_target'] == "Url" and driver.current_url == \
                                    params['match_content']:
                                target_handle = handle
                                break
                        elif params['match_type'] == "contains":
                            if target_handle is None and params['match_target'] == "Title" and params['match_content'] in driver.title:
                                target_handle = handle
                                break
                            if target_handle is None and params['match_target'] == "Url" and params['match_content'] in driver.current_url:
                                target_handle = handle
                                break
                else:
                    raise Exception('浏览器没有标签页')
            elif params['match_type_select'] == "handle":
                target_handle = params['handle']

        if target_handle is None:
            raise Exception('没有找到匹配的标签页')
        else:
            driver.switch_to_window(target_handle)
            driver.close()
    except Exception as e:
        raise e


def getBrowserRunStatus(params):
    status = False
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            # driver.get('http://www.baidu.com/')
            window_handles = driver.window_handles
            if len(window_handles) > 0:
                status = True
            else:
                status = False
    except Exception:
        status = False

    return status


def browserForward(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.forward()
    except Exception as e:
        raise e


def browserBack(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.back()
    except Exception as e:
        raise e


def browserRefresh(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.refresh()
    except Exception as e:
        raise e


def stopLoadPage(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.set_page_load_timeout(0)
    except Exception as e:
        raise e


def waitForPageLoaded(params):
    try:
        result = False
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'timeout' not in params.keys() or params['timeout'] == "":
            raise Exception("缺少参数：等待时间")
        else:
            driver = params['browser_info']
            try:
                timeout = int(params['timeout'])
            except ValueError:
                raise ValueError("等待时间只能为整数或小数")
            if params['element_type'] == 'UI':
                if 'html' not in params['element_info'].keys():
                    raise Exception('检测元素为非网页元素')
                html = params['element_info']['html']
                xpath = generate_xpath(html=html)
                for _ in range(timeout):
                    try:
                        driver.find_element_by_xpath(xpath)
                        result = True
                        break
                    except Exception as e:
                        print(e)
                    time.sleep(1)
            elif params['element_type'] == 'Xpath':
                for _ in range(timeout):
                    try:
                        driver.find_element_by_xpath(params['element_xpath'])
                        result = True
                        break
                    except Exception as e:
                        print(e)
                    time.sleep(1)
            else:
                raise Exception("暂不支持当前元素类型")
        return result
    except Exception as e:
        raise e


def downloadFile(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'download_url' not in params.keys() or params['download_url'] is None:
            raise Exception('缺少参数：下载链接')
        elif 'save_path' not in params.keys() or params['save_path'] is None:
            raise Exception('缺少参数：保存路径')
        elif 'is_async_download' not in params.keys() or params['is_async_download'] is None:
            raise Exception('缺少参数：是否同步下载')
        else:
            pass
    except Exception as e:
        raise e


def getPageUrl(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            return driver.current_url
    except Exception as e:
        raise e


def getPageTitle(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            return driver.title
    except Exception as e:
        raise e


def setPageCookies(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'cookies' not in params.keys() or params['cookies'] is None:
            raise Exception("缺少参数：Cookies数据")
        else:
            driver = params['browser_info']
            if not isinstance(params['cookies'], dict):
                try:
                    params['cookies'] = json.loads(params['cookies'])
                except json.decoder.JSONDecodeError:
                    raise Exception("cookies类型错误，cookies必须是字典类型或json字符串")
                # raise Exception("cookies类型错误，cookies必须是字典类型或json字符串")
            if "name" not in params['cookies'].keys() and "value" not in params['cookies'].keys():
                raise Exception("cookies的key必须包含name和value！")
            driver.add_cookie(params['cookies'])
            # driver.add_cookie(params['cookies'])
    except Exception as e:
        raise e


def browserCapture(params):
    # from PIL import ImageGrab, Image
    from PIL import Image
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'capture_type' not in params.keys() or params['capture_type'] is None:
            raise Exception("缺少参数：截图方式")
        elif 'save_path' not in params.keys() or params['save_path'] is None:
            raise Exception("缺少参数：保存路径")
        elif 'file_name' not in params.keys() or params['file_name'] is None:
            raise Exception("缺少参数：保存文件名")
        elif params['capture_type'] == 'Element' and (
                'target_element' not in params.keys() or params['target_element'] is None):
            raise Exception("缺少参数：网页元素")
        else:
            driver = params['browser_info']
            img_file_path = '%s\\%s.png' % (
                params['save_path'], params['file_name'])
            if params['capture_type'] == 'Window':
                driver.get_screenshot_as_file(img_file_path)
            elif params['capture_type'] == 'Area':
                full_image_path = '%s\\full_image.png' % params['save_path']
                driver.get_screenshot_as_file(full_image_path)
                img = Image.open(full_image_path)
                cropped = img.crop((int(params['x']), int(params['y']), int(params['x']) + int(params['width']),
                                    int(params['y']) + int(params['height'])))
                cropped.save(img_file_path)
            elif params['capture_type'] == 'Element':
                html = params['target_element']['html']
                xpath = generate_xpath(html=html)
                el = driver.find_element_by_xpath(xpath)
                el.screenshot(img_file_path)
            elif params['capture_type'] == 'xpath':
                im_info = driver.find_element_by_xpath(params['xpath']).screenshot_as_base64
                im_base64 = im_info  # 拿到base64编码的图片信息
                im_bytes = base64.b64decode(im_base64)  # 转为bytes类型
                with open(img_file_path, 'wb') as f:  # 保存图片到本地
                    f.write(im_bytes)

                # im = ImageGrab.grab(bbox=(location['x'], \
                #     location['y'], \
                #     location['x'] + rect['width'], \
                #     location['y'] + rect['height']))

                # im.save(img_file_path)
            else:
                raise Exception('该截图方式尚未支持')
            return img_file_path
    except Exception as e:
        raise e


def getScrollPosition(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            scroll_pos = driver.execute_script("""
                return {
                    scrollTop: document.documentElement.scrollTop,
                    scrollLeft: document.documentElement.scrollLeft
                }
            """)

            return scroll_pos
    except Exception as e:
        raise e


def setScrollPosition(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'scroll_type' not in params.keys() or params['scroll_type'] is None:
            raise Exception("缺少参数：滚动方向")
        elif 'scroll_position' not in params.keys() or params['scroll_position'] is None:
            raise Exception("缺少参数：滚动位置")
        else:
            driver = params['browser_info']

            if params['scroll_type'] == 'Horizontal':
                driver.execute_script(
                    'window.scrollBy(%s - document.documentElement.scrollLeft, 0)' % params['scroll_position'])
            elif params['scroll_type'] == 'Vertical':
                driver.execute_script(
                    'window.scrollBy(0, %s - document.documentElement.scrollTop)' % params['scroll_position'])
            else:
                pass

            return None
    except Exception as e:
        raise e


def switchToIframe(params):
    try:
        if 'switch_type' not in params.keys():
            if 'browser_info' not in params.keys() or params['browser_info'] is None:
                raise Exception("缺少参数：浏览器对象")
            elif 'element_no' not in params.keys() or params['element_no'] is None:
                raise Exception("缺少参数：元素序号")
            else:
                driver = params['browser_info']

                iframes = driver.find_elements_by_tag_name('iframe')
                frames = driver.find_elements_by_tag_name('frame')
                if len(iframes) > 0 or len(frames) > 0:
                    driver.switch_to.frame(int(params['element_no']) - 1)
                else:
                    raise Exception('当前页面不存在iframe元素')
        else:
            if 'browser_info' not in params.keys() or params['browser_info'] is None:
                raise Exception("缺少参数：浏览器对象")
            else:
                driver = params['browser_info']

                if params['switch_type'] == 'Index':
                    iframes = driver.find_elements_by_tag_name('iframe')
                    frames = driver.find_elements_by_tag_name('frame')
                    if len(iframes) > 0 or len(frames) > 0:
                        driver.switch_to.frame(int(params['element_no']) - 1)
                    else:
                        raise Exception('当前页面不存在iframe元素')
                elif params['switch_type'] == 'Xpath':
                    element = driver.find_element_by_xpath(params['element_xpath'])
                    if element is not None:
                        driver.switch_to.frame(element)
    except Exception as e:
        raise e


def switchDefaultContent(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        else:
            driver = params['browser_info']
            driver.switch_to.default_content()
    except Exception as e:
        raise e


def switchToWindow(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'window_no' not in params.keys() or params['window_no'] is None:
            raise Exception("缺少参数：元素序号")
        else:
            driver = params['browser_info']
            driver.switch_to_window(driver.window_handles[int(params['window_no']) - 1])
            return driver
    except Exception as e:
        raise e


def selectClick(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'xpath' not in params.keys() or params['xpath'] is None:
            raise Exception("缺少参数：元素XPATH")
        elif 'selectedText' not in params.keys() or params['selectedText'] is None:
            raise Exception("缺少参数：选择项")
        else:
            driver = params['browser_info']
            driver.find_element_by_xpath(params['xpath']).click()
            time.sleep(.2)
            driver.find_element_by_xpath("//option[text()='" + params['selectedText'] + "']").click()
            return None
    except Exception as e:
        raise e


def select_tr_Click(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'xpath' not in params.keys() or params['xpath'] is None:
            raise Exception("缺少参数：元素XPATH")
        else:
            driver = params['browser_info']
            table = driver.find_element_by_xpath(params['xpath'])
            trs = table.find_elements_by_xpath('.//tbody/tr')
            for tr in trs:
                if params['selectedText']:
                    if tr.text.split('\n')[0] in params['selectedText'].split(','):
                        tr.click()
                        time.sleep(.2)
                else:
                    tr.click()
                    time.sleep(.2)
    except Exception as e:
        raise e


def resolvingListData_(params):
    try:
        if params['if_headers'] == '是':
            result = {
                "headers": [],
                "items": []
            }
        else:
            result = {
                "items": []
            }
        if 'browser_info' in params.keys() and params['browser_info'] is not None \
                and 'css_selector' in params.keys() and params['css_selector'] is not None:
            driver = params["browser_info"]
            css_selector = params["css_selector"]
            headers = []
            items = []
            element = driver.find_element_by_css_selector(css_selector)
            thead = element.find_element_by_tag_name("thead")

            ths = element.find_elements_by_tag_name("th")
            ignore_index = []
            column_index = []
            if ths is not None and len(ths) > 0:
                for i, th in enumerate(ths):
                    colspan = th.get_attribute("colspan")
                    if colspan:
                        start = column_index[len(column_index) - 1] if len(column_index) > 0 else 0
                        for j in range(start + 1, start + int(colspan) + 1):
                            column_index.append(j)
                            ignore_index.append(j)
                    else:
                        column_index.append(i)
                        headers.append(th.text.replace("↓", "").replace("\n", ""))
            if params["if_headers"] == '是':
                result['headers'] = headers

            tbody = element.find_element_by_tag_name("tbody")
            # trs = tbody.find_elements_by_tag_name("tr")
            trs = tbody.find_elements_by_xpath('./tr')
            if trs and len(trs) > 0:
                for tr in trs:
                    if tr.get_attribute("class"):
                        name = tr.text.split('\n')[0]
                        if name in params['ignore_column']:
                            temp_item = []
                            temp_data = {}
                            tds = tr.find_elements_by_tag_name("td")
                            if tds and len(tds) > 0:
                                for i, td in enumerate(tds):
                                    temp_item.append(td.text)
                                temp_data[name] = temp_item
                                temp_data[name][-1] = '0'
                                items.append(temp_data)
                                # items.append(temp_item)
                    else:
                        trs_ = tr.find_elements_by_tag_name("tr")
                        for tr in trs_:
                            name_ = tr.text.split('\n')[0]
                            if name_ in params['ignore_column'] or name_ in params['contain'].split(',')[0]:
                                temp_item = []
                                temp_data = {}
                                tds = tr.find_elements_by_tag_name("td")
                                if tds and len(tds) > 0:
                                    for i, td in enumerate(tds):
                                        temp_item.append(td.text)
                                    if name_ in params['contain'].split(',')[0]:
                                        for n in items:
                                            if name in n:
                                                n[name][-1] = temp_item[int(params['contain'].split(',')[1])]
                                    else:
                                        temp_data[name_] = temp_item
                                        temp_data[name_][-1] = '0'
                                    items.append(temp_data)
            result["items"] = items
            if params['add_column']:
                result["items"].append(params['add_column'])

            items_list = []
            for r in result['items']:
                for k, y in r.items():
                    items_list.append(y)
            result['items'] = items_list
            return result
        else:
            raise Exception(message="缺少参数！")
    except Exception as e:
        raise e


def canvas_Click(params):
    try:
        if 'browser_info' not in params.keys() or params['browser_info'] is None:
            raise Exception("缺少参数：浏览器对象")
        elif 'xpath' not in params.keys() or params['xpath'] is None:
            raise Exception("缺少参数：元素XPATH")
        elif 'xpath' not in params.keys() or params['x'] is None:
            raise Exception("缺少参数：x坐标")
        elif 'xpath' not in params.keys() or params['y'] is None:
            raise Exception("缺少参数：y坐标")
        else:
            driver = params['browser_info']
            aa = driver.find_element_by_xpath(params['xpath'])
            ActionChains(driver).move_to_element_with_offset(aa, int(params['x']), int(params['y'])).click().perform()
    except Exception as e:
        raise e


def browser_minimize(params):
    driver = params['browser_info']
    driver.minimize_window()


def maximize(params):
    if 'browser_info' in params.keys() and params['browser_info'] is not None:
        driver = params['browser_info']
        driver.maximize_window()
    else:
        raise Exception('没有启动浏览器')


if __name__ == "__main__":
    # switchBrowserTag({
    #     'browser_info': {
    #         'executor_url': 'http://127.0.0.1:51512',
    #         'session_id': '44f7e19d35aec4c9d442117af1b14f91'
    #     },
    #     'match_target': "Title",
    #     'match_content': "设置"
    # })
    print(sys.platform)
    # driver = openBrowser({
    #     'browser_type': 'Microsoft Edge',
    #     'uiauto_config': {
    #         "client_dir": r'D:\develop\UiAuto'
    #     },
    #     'url': 'http://www.baidu.com',
    #     'is_stealth': False,
    #     'is_headless': False
    # })
    #
    # # switchToIframe({
    # #     'browser_info': driver,
    # #     'element_no': '1'
    # # })
    #
    # # driver.find_element_by_xpath('//input[@id="kw"]').send_keys('Python')
    # print(dir(driver))
    # print(driver.current_url)
    # time.sleep(5)
    # goto_url({
    #     "browser_info": driver,
    #     "url": "http://360.com"
    # })
    # # driver.quit()
    # closeBrowser({
    #     "browser_info": driver
    # })
