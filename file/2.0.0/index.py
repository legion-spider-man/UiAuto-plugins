import codecs
import os
import shutil
import requests


def readFile(params):
    path = params['path']
    cont = ''
    code = params['code']
    try:
        with codecs.open(path, 'r', code) as file:
            # line = file.readline()
            data = file.readlines()
            for line in data:
                cont = cont + line
    except Exception as e:
        raise e
    return cont


def writeFile(params):
    path = params['path']
    code = params['code']
    try:
        with codecs.open(path, 'w', code) as file:
            file.write(str(params['content']))
    except Exception as e:
        raise e
    return


def addWriteFile(params):
    path = params['path']
    code = params['code']
    try:
        with codecs.open(path, 'a', code) as file:
            file.write('\n' + str(params['content']))
    except Exception as e:
        raise e
    return


def getSize(params):
    path = params['path']
    try:
        size = os.path.getsize(path)
        size = size / float(1024 * 1024)
        size = round(size, 2)
    except Exception as err:
        raise err
    return "%.2f MB" % (size)


def copyFile(params):
    path = params['path']
    tarDir = params['tarPath']
    cover = params['cover']
    *_, filename = os.path.split(path)
    target_path = '%s/%s' % (tarDir, filename)
    try:
        if 'yes' == cover:
            if os.path.exists(target_path):
                shutil.copyfile(path, '%s/%s.tempfile' % (tarDir, filename))
                os.remove(target_path)
                os.rename('%s/%s.tempfile' % (tarDir, filename), target_path)
            else:
                shutil.copy(path, tarDir)
        elif 'no' == cover:
            if os.path.exists(target_path):
                shutil.copyfile(path, '%s/副本_%s' % (tarDir, filename))
                return "文件夹已存在同名文件"
            else:
                shutil.copy(path, tarDir)
    except Exception as e:
        raise e


def moveFile(params):
    path = params['path']
    tarDir = params['tarPath']
    cover = params['cover']
    *_, filename = os.path.split(path)
    params['path'] = tarDir
    target_path = '%s/%s' % (tarDir, filename)
    try:
        if 'no' == cover:
            if os.path.exists(target_path):
                shutil.move(path, '%s/副本_%s' % (tarDir, filename))
            else:
                shutil.move(path, target_path)
        elif 'yes' == cover:
            if os.path.exists(target_path):
                os.remove(target_path)
            shutil.move(path, target_path)

    except Exception as e:
        raise e


def newName(params):
    path = params['path']
    new_name = params['newName']
    ext = os.path.splitext(path)[1]
    try:
        filePath, old_name = os.path.split(path)
        os.rename(path, filePath + "/" + new_name + ext)
    except Exception as err:
        raise err
    return


def delFile(params):
    path = params['path']
    try:
        if os.path.exists(path):
            os.remove(path)
            return "删除成功"
        else:
            return "文件不存在"
    except Exception as e:
        raise e


def createDir(params):
    path = params['path']
    dirName = params['name']
    try:
        if not os.path.exists(os.path.join(path, dirName)):
            os.mkdir(os.path.join(path, dirName))
            return (os.path.join(path, dirName))
        else:
            return (os.path.join(path, dirName))
    except Exception as e:
        raise e


def createFile(params):
    path = params['path']
    fileName = params['name']
    try:
        if os.path.exists(path + "/" + fileName):
            return (path + "/" + fileName)
        else:
            with open(path + "/" + fileName, 'w+') as file:
                pass
            return (path + "/" + fileName)
    except Exception as e:
        raise e


def fileIsExist(params):
    path = params['path']
    data = None
    try:
        if os.path.exists(path):
            data = True
        else:
            data = False
    except Exception as e:
        raise e
    return data


def pathIsExist(params):
    path = params['path']
    data = None
    try:
        if os.path.exists(path):
            data = True
        else:
            data = False
    except Exception as e:
        raise e
    return data


def list_files(params):
    path = params['path']
    result = []
    try:
        for file in os.listdir(path):
            if os.path.isfile(os.path.join(path, file)):
                result.append(os.path.join(path, file))
    except Exception as e:
        raise e
    return result


def list_dirs(params):
    path = params['path']
    result = []
    try:
        for dir in os.listdir(path):
            if os.path.isdir(os.path.join(path, dir)):
                result.append(os.path.join(path, dir))
    except Exception as e:
        raise e
    return result


def findFile(params):
    path = params['path']
    fileName = params['name']
    deep = params['deep']
    result = False
    try:
        if 'yes' == deep:
            for main_path, dirs, files in os.walk(path):
                if fileName in files:
                    result = os.path.join(main_path, fileName)
                    break
        elif 'no' == deep:
            for file in list_files(params):
                print(file)
                filepath, file_name = os.path.split(file)
                if file_name == fileName:
                    result = file
                    break
    except Exception as e:
        raise e
    return result


def getFileName(params):
    if 'file_path' not in params.keys() or params['file_path'] is None or params['file_path'] == '':
        raise Exception('缺少参数：文件路径')
    else:
        basename = os.path.basename(params["file_path"])
        return basename.split(".")[0]


if __name__ == "__main__":
    # saveImageFromUrl({'download_url':'https://img0.baidu.com/it/u=2862534777,914942650&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500', 'image_path':'E://hwx', 'image_name':'huangkjsfkd.png'} )
    pass