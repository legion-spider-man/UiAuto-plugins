import json
import os
import socket
import subprocess
import sys
import time

from threading import Thread


# 录屏状态服务
def open_server(params):
    try:
        sock = socket.socket(type=socket.SOCK_DGRAM)
        sock.bind(('127.0.0.1', 3344))  # 绑定地址
        ffmpeg_dir = os.path.join(os.path.dirname(__file__), 'ffmpeg.exe')

        save_path = params.get('save_path', True)
        if 'win' not in sys.platform:
            raise Exception('当前只适配了Windows平台')
        elif not save_path:
            raise Exception('保存路径为输入')
        elif save_path.split('.')[-1].upper() != 'MP4':
            raise Exception('文件后缀必须是mp4')
        os.environ['ffmpeg'] = ffmpeg_dir
        if not os.path.exists(os.path.dirname(save_path)):
            os.makedirs(os.path.dirname(save_path))
        # 录屏时长
        duration = '' if params.get('screen_recording_duration', 0) == 0 else '-t ' + params[
            'screen_recording_duration']
        screen_recording_duration = int(params.get('screen_recording_duration', 0))
        # 录制帧率
        fps = int(params.get('fps', 21))
        cmd = rf'{ffmpeg_dir} -y -f gdigrab -i desktop -r {str(fps)} -crf 24 {str(duration)} -pix_fmt yuv420p -preset:v veryfast -tune:v zerolatency {save_path}'
        screenrecording_terminal = subprocess.Popen(*(cmd,), shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        # 实现通信循环
        start_date = time.time()
        while True:
            data, addr = sock.recvfrom(1024)
            data = data.decode('utf-8')
            data = json.loads(data)
            # 接收状态指令
            status = data['status']
            if status == 'end':
                print('正在关闭录屏')
                screenrecording_terminal.stdin.write(b'q')
                screenrecording_terminal.stdin.flush()
                time.sleep(1)
                screenrecording_terminal.kill()
                break
            if screen_recording_duration > 0 and time.time() > start_date + screen_recording_duration:
                print('正在关闭录屏')
                screenrecording_terminal.stdin.write(b'q')
                screenrecording_terminal.stdin.flush()
                time.sleep(1)
                screenrecording_terminal.kill()
                break
            time.sleep(.5)
        sock.close()
    except Exception as e:
        status = 'end'

        return e


# 发送指令給open_server
def send_message(message: dict):
    try:
        sock = socket.socket(type=socket.SOCK_DGRAM)
        sock.connect(('127.0.0.1', 3344))
        messages = json.dumps(message).encode('utf-8')
        sock.sendto(messages, ('127.0.0.1', 3344))
        sock.close()
    except Exception as e:
        print(e)
        return e


def start_server(params):
    open_server_thread = Thread(target=open_server, args=(params,))
    open_server_thread.start()
    pass
    #open_server_thread.start()
# 创建socket服务的线程

print("open_server_thread线程开启")


def stop_server(params):
    try:
        send_message({'status': 'end'})
        return "关闭录像成功"
    except Exception as e:
        raise e


if __name__ == '__main__':
    params = {"screen_recording_duration": "20", "fps": "21", "save_path": "C:\\Users\\王迪\\Desktop\\tools.mp4"}
    start_server(params)
    stop_server({})
    #time.sleep(3)
    #stop_server(params)
