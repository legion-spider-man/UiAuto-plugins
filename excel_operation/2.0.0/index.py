import re
import os
import xlwings as xw
from xlwings.utils import rgb_to_int
import win32gui
import datetime
from xlwings.conversion import Converter
from openpyxl import Workbook, load_workbook
import pandas as pd
import xlrd


def sheetIsExists(excel, sheet):
    data = {
        "excel_handle": excel
    }
    sheetNames = getSheetsName(data)
    if sheet not in sheetNames:
        return False
    return True


def excelValueTo(value):
    newValue = value
    if newValue is None:
        newValue = ''
    if type(newValue) == datetime.datetime:
        newValue = str(newValue)
    if type(newValue) == float:
        if newValue == round(newValue):
            newValue = round(newValue)
        return newValue


class UConverter(Converter):

    @staticmethod
    def read_value(cList, v):
        dList = cList
        if type(cList) == list:
            if type(cList[0]) == list:
                lengthCList = len(cList)
                length = len(cList[0])
                for c in range(lengthCList):
                    for l in range(length):
                        dList[c][l] = excelValueTo(
                            dList[c][l])

            else:
                lengthCList = len(cList)
                for c in range(lengthCList):
                    dList[c] = excelValueTo(
                        dList[c])

        else:
            dList = excelValueTo(dList)
        return dList


def convertList(vList):
    num = 0
    for v in vList:
        v = v.upper()
        num = num * 26 + ord(v) - ord('A') + 1

    return num


def findStr(string):
    string = string.upper()
    return re.findall('[0-9]+|[A-Z]+', string)


def exist(path):
    isExist = False
    if os.path.exists(path):
        isExist = True
    return isExist


"""
TODO:
    - 使用Office软件打开Excel文件
"""


def open_excel(params):
    path = params['path']
    try:
        if 'path' not in params.keys() or params['path'] is None or params['path'] == '':
            raise Exception('缺少参数：文件路径')
        else:
            if not os.path.exists('\\'.join(params['path'].split('\\')[:-1])):
                os.makedirs('\\'.join(params['path'].split('\\')[:-1]))
                if not os.path.exists(params['path']):
                    Workbook()
            visible = False if params['visible'] == 'no' else True
            print(visible)
            app = xw.App(visible=visible, add_book=False)
            app.display_alerts = False
            app.api.AskToUpdateLinks = False
            if os.path.exists(params['path']):
                excel = app.books.open(path)
            else:
                excel = app.books.add()
                excel.save(path)

        #           win32gui.SetForegroundWindow(app.hwnd)
        # win32gui.ShowWindow(app.hwnd, win32con.SW_MAXIMIZE)

        return excel
    except Exception as e:
        raise e


"""
TODO:
    - 保存指定excel文件
"""


def save(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        else:
            excel = params['excel_handle']
            excel.save()
    except Exception as e:
        raise e


def bindBook(params):
    fileName = params['excelName']
    newExcel = None
    try:
        books = xw.books
        for book in books:
            if book.name == fileName:
                newExcel = book
                break
        if newExcel is None:
            raise Exception("绑定Excel失败")
        excel = newExcel

        return excel
    except AttributeError:
        raise Exception("绑定Excel失败，没有安装MicrosoftExcel")
    except Exception:
        raise Exception("绑定Excel出错")


def saveAs(params):
    try:
        file_name = ''
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'save_dir' not in params.keys() or params['save_dir'] is None or params['save_dir'] == '':
            raise Exception('缺少参数：保存目录')
        else:
            excel = params['excel_handle']
            if 'save_file_name' not in params.keys() or params['save_file_name'] is None or params[
                'save_file_name'] == '':
                file_name = excel.name
            else:
                file_name = params['save_file_name']
            excel.save(params['save_dir'] + "/" + file_name)

            return True
    except Exception as e:
        raise e


def activeBook(params):
    global excel
    try:
        excel.activate()
        win32gui.SetForegroundWindow(excel.app.hwnd)
    except Exception as e:
        raise e


def close(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        else:
            excel = params['excel_handle']
            if params['is_save'] == 'yes':
                excel.save()
            excel.app.quit()
    except Exception as e:
        raise e


def readCell(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'excel_sheet' not in params.keys() or params['excel_sheet'] is None or params['excel_sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'excel_cell' not in params.keys() or params['excel_cell'] is None or params['excel_cell'] == '':
            raise Exception('缺少参数：指定单元格')
        else:
            excel = params['excel_handle']
            sheet = params['excel_sheet']
            # cell = formatCell(params['excel_cell'])
            cell = params['excel_cell']
            if not sheetIsExists(excel, sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, sheet))
            else:
                dataSheet = excel.sheets[sheet]
                if type(cell) == list:
                    data = dataSheet.range((int(cell[0]), int(cell[1]))).value
                else:
                    data = dataSheet.range(cell).value

            return data
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def readRange(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'range' not in params.keys() or params['range'] is None or params['range'] == '':
            raise Exception('缺少参数：指定单元格')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # excel_range = formatRange(params['range'])
            excel_range = params['range']
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            else:
                data = None
                dataSheet = excel.sheets[excel_sheet]
                if type(excel_range) == list:
                    data = dataSheet.range((int(excel_range[0][0]), int(excel_range[0][1])), (int(
                        excel_range[1][0]), int(excel_range[1][1]))).value
                else:
                    data = dataSheet.range(excel_range).value

                if type(data[0]) != list:
                    data = [data]
                # return formatList(data)
                return data
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def readRow(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'row' not in params.keys() or params['row'] is None or params['row'] == '':
            raise Exception('缺少参数：读取指定单元格所在的行')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # row = formatCell(params['row'])
            row = params['row']
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            data = list()
            dataSheet = excel.sheets[excel_sheet]
            used_range = dataSheet.used_range
            column = used_range.last_cell.column
            if type(row) == list:
                data = dataSheet.range(
                    (int(row[0]), int(row[1])), (int(row[0]), column)).value
            else:
                a, b = findStr(row)
                b = int(b)
                c = convertList(a)
                data = dataSheet.range((b, c), (b, column)).value

            # return formatList(data)
            return data
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def readCol(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'col' not in params.keys() or params['col'] is None or params['col'] == '':
            raise Exception('缺少参数：指定单元格')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # col = formatCell(params['col'])
            col = params['col']
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            data = None
            dataSheet = excel.sheets[excel_sheet]
            used_range = dataSheet.used_range
            row = used_range.last_cell.row
            if type(col) == list:
                data = dataSheet.range(
                    (int(col[0]), int(col[1])), (row, int(col[1]))).value
            else:
                a, b = findStr(col)
                b = int(b)
                c = convertList(a)
                data = dataSheet.range((b, c), (row, c)).value

            # return formatList(data)
            return data
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def getRowNum(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            dataSheet = excel.sheets[excel_sheet]
            used_range = dataSheet.used_range
            row = used_range.last_cell.row
            return row
    except Exception as e:
        raise e


def getColNum(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            dataSheet = excel.sheets[excel_sheet]
            used_range = dataSheet.used_range
            column = used_range.last_cell.column
            return column
    except Exception as e:
        raise e


def mergeCell(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            # mergeCell = formatRange(params['mergeCell'])
            mergeCell = params['cell']
            isMerge = params['isMerge']
            isSave = params['isSave']
            ex = None
            dataSheet = excel.sheets[excel_sheet]
            if type(mergeCell) == list:
                ex = dataSheet.range((int(mergeCell[0][0]), int(
                    mergeCell[0][1])), (int(mergeCell[1][0]), int(mergeCell[1][1])))
            else:
                ex = dataSheet.range(mergeCell)

            # ex.api.merge
            try:
                if isMerge == 'merge':
                    ex.api.Merge()
                else:
                    ex.api.UnMerge()
            except Exception as ex:
                raise ex
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as exc:
        raise exc


def writeCell(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        elif 'text' not in params.keys() or params['text'] is None or params['text'] == '':
            raise Exception('缺少参数：写入内容')
        else:
            excel = params['excel_handle']
            text = params['text']
            isSave = params['isSave']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            dataSheet = excel.sheets[excel_sheet]
            if type(cell) == list:
                dataSheet.range((int(cell[0]), int(cell[1]))).value = text
            else:
                dataSheet.range(cell).value = text
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def writeRow(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        elif 'data' not in params.keys() or params['data'] is None or params['data'] == '':
            raise Exception('缺少参数：写入内容')
        else:
            excel = params['excel_handle']
            data = params['data']
            isSave = params['isSave']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            dataSheet = excel.sheets[excel_sheet]
            if type(cell) == list:
                dataSheet.range((int(cell[0]), int(cell[1]))).value = data
            else:
                dataSheet.range(cell).value = data
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def deleteRow(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格所在的行')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            cell = params['cell']
            isSave = params['isSave']
            if cell.isdigit():
                cell = int(cell)

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if isinstance(cell, int):
                dataSheet.range((cell, 1)).api.EntireRow.Delete()
            elif isinstance(cell, list):
                dataSheet.range(
                    (int(cell[0]), int(cell[1]))).api.EntireRow.Delete()
            else:
                dataSheet.range(cell).api.EntireRow.Delete()
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def writeCol(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定开始单元格')
        elif 'data' not in params.keys() or params['data'] is None or params['data'] == '':
            raise Exception('缺少参数：写入内容')
        else:
            excel = params['excel_handle']
            data = params['data']
            isSave = params['isSave']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if type(cell) == list:
                dataSheet.range((int(cell[0]), int(cell[1]))).options(
                    transpose=True).value = data
            else:
                dataSheet.range(cell).options(transpose=True).value = data
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def deleteCol(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：单元格或列号')
        else:
            excel = params['excel_handle']

            excel_sheet = params['sheet']
            cell = params['cell']
            isSave = params['isSave']

            if isinstance(cell, str) and cell.isdigit():
                cell = int(cell)

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if isinstance(cell, int):
                dataSheet.range((1, cell)).api.EntireColumn.Delete()
            elif isinstance(cell, list):
                dataSheet.range(
                    (int(cell[0]), int(cell[1]))).api.EntireColumn.Delete()
            else:
                dataSheet.range(cell).api.EntireColumn.Delete()
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def insertRow(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：在指定单元格所在行插入')
        elif 'data' not in params.keys() or params['data'] is None or params['data'] == '':
            raise Exception('缺少参数：写入内容')
        else:
            excel = params['excel_handle']
            data = params['data']
            isSave = params['isSave']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if type(cell) == list:
                dataSheet.range(
                    (int(cell[0]), int(cell[1]))).api.EntireRow.Insert()
                dataSheet.range((int(cell[0]), int(cell[1]))).value = data
            else:
                dataSheet.range(cell).api.EntireRow.Insert()
                dataSheet.range(cell).value = data
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def insertCol(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：在指定单元格所在列插入')
        elif 'data' not in params.keys() or params['data'] is None or params['data'] == '':
            raise Exception('缺少参数：写入内容')
        else:
            excel = params['excel_handle']
            data = params['data']
            isSave = params['isSave']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if type(cell) == list:
                dataSheet.range(
                    (int(cell[0]), int(cell[1]))).api.EntireColumn.Insert()
                dataSheet.range((int(cell[0]), int(cell[1]))).options(
                    transpose=True).value = data
            else:
                dataSheet.range(cell).api.EntireColumn.Insert()
                dataSheet.range(cell).options(transpose=True).value = data
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def insertPicture(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'picture' not in params.keys() or params['picture'] is None or params['picture'] == '':
            raise Exception('缺少参数：图片路径')
        elif 'picName' not in params.keys() or params['picName'] is None or params['picName'] == '':
            raise Exception('缺少参数：图片名字')
        elif 'left' not in params.keys() or params['left'] is None or params['left'] == '':
            raise Exception('缺少参数：左边距')
        elif 'top' not in params.keys() or params['top'] is None or params['top'] == '':
            raise Exception('缺少参数：上边距')
        elif 'width' not in params.keys() or params['width'] is None or params['width'] == '':
            raise Exception('缺少参数：宽度')
        elif 'height' not in params.keys() or params['height'] is None or params['height'] == '':
            raise Exception('缺少参数：高度')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            picPath = params['picture']
            picName = params['picName']
            left = float(params['left'])
            top = float(params['top'])
            width = float(params['width'])
            height = float(params['height'])

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            pictures = dataSheet.pictures
            if picName.strip(' ') == '':
                pictures.add(image=picPath, left=left, top=top,
                             width=width, height=height)
            else:
                pictures.add(image=picPath, name=picName, left=left,
                             top=top, width=width, height=height, update=True)
    except Exception as e:
        raise e


def deletePicture(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'name' not in params.keys() or params['name'] is None or params['name'] == '':
            raise Exception('缺少参数：图片名字或序号')
        else:
            excel = params['excel_handle']
            name = params['name']
            excel_sheet = params['sheet']
            if name.isdigit():
                name = int(name)

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            pictures = dataSheet.pictures
            if type(name) == int:
                pictures[name].delete()
            else:
                for picture in pictures:
                    if picture.name == name:
                        picture.delete()
                        break
    except IndexError:
        raise Exception("图片下标超出范围")
    except Exception as e:
        raise e


def writeRange(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：开始单元格')
        elif 'data' not in params.keys() or params['data'] is None or params['data'] == '':
            raise Exception('缺少参数：写入内容')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']
            data = params['data']
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataLength = len(data)
            length = len(data[0])
            count = 1
            dataSheet = excel.sheets[excel_sheet]
            for dl in range(dataLength):
                l = len(data[dl])
                if length < l:
                    length = l
                elif count > l:
                    count = l
            if length != count:
                for dl in range(dataLength):
                    l = len(data[dl])
                    ls = length - l
                    if ls != 0:
                        for lss in range(ls):
                            data[dl].append('')
            if type(cell) == list:
                dataSheet.range((int(cell[0]), int(cell[1]))).value = data
            else:
                dataSheet.range(cell).value = data
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def selectRange(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'range' not in params.keys() or params['range'] is None or params['range'] == '':
            raise Exception('缺少参数：指定区域')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # excel_range = formatRange(params['range'])
            excel_range = params['range']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            ActiveSheet(excel, excel_sheet)
            select = None
            if type(excel_range) == list:
                select = dataSheet.range((int(excel_range[0][0]), int(excel_range[0][1])),
                                         (int(excel_range[1][0]), int(excel_range[1][1])))
            else:
                select = dataSheet.range(excel_range)
            select.select()
    except ValueError:
        raise Exception("范围格式不正确")
    except Exception as e:
        raise e


def ActiveSheet(excel, sheet):
    try:
        dataSheet = excel.sheets[sheet]
        dataSheet.select()
    except Exception:
        pass


def activeSheet(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            ActiveSheet(excel, excel_sheet)
    except Exception as e:
        raise e


def clearRange(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'range' not in params.keys() or params['range'] is None or params['range'] == '':
            raise Exception('缺少参数：指定区域')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # excel_range = formatRange(params['range'])
            excel_range = params['range']
            isSave = params['isSave']
            isClearFormat = params['isClearFormat']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if type(excel_range) == list:
                excel_range = dataSheet.range((int(excel_range[0][0]), int(excel_range[0][1])),
                                              (int(excel_range[1][0]), int(excel_range[1][1])))
            else:
                excel_range = dataSheet.range(excel_range)
            if isClearFormat == 'yes':
                excel_range.clear()
            else:
                excel_range.clear_contents()
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("范围格式不正确")
    except Exception as e:
        raise e


def deleteRange(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'range' not in params.keys() or params['range'] is None or params['range'] == '':
            raise Exception('缺少参数：指定区域')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # excel_range = formatRange(params['range'])
            excel_range = params['range']
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if type(excel_range) == list:
                excel_range = dataSheet.range((int(excel_range[0][0]), int(
                    excel_range[0][1])), (int(excel_range[1][0]), int(excel_range[1][1])))
            else:
                excel_range = dataSheet.range(excel_range)
            excel_range.api.Delete()
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("范围格式不正确")
    except Exception as e:
        raise e


def setColWidth(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        elif 'colWidth' not in params.keys() or params['colWidth'] is None or params['colWidth'] == '':
            raise Exception('缺少参数：列宽值')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']
            colWidth = params['colWidth']
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if type(cell) == list:
                if type(cell[0]) == list:
                    cell = dataSheet.range((int(cell[0][0]), int(
                        cell[0][1])), int((cell[1][0]), int(cell[1][1])))
                else:
                    cell = dataSheet.range((int(cell[0]), int(cell[1])))
            else:
                cell = dataSheet.range(cell)
            if type(colWidth) == float or type(colWidth) == int:
                if 0 <= colWidth <= 255:
                    cell.column_width = colWidth
                else:
                    cell.columns.autofit()
                if isSave == 'yes':
                    excel.save()
            else:
                raise Exception("列宽错误")
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def setRowHeight(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        elif 'rowHeight' not in params.keys() or params['rowHeight'] is None or params['rowHeight'] == '':
            raise Exception('缺少参数：行高值')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']
            rowHeight = params['rowHeight']
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if type(cell) == list:
                if type(cell[0]) == list:
                    cell = dataSheet.range((int(cell[0][0]), int(cell[0][1])),
                                           (int(cell[1][0]), int(cell[1][1])))
                else:
                    cell = dataSheet.range((int(cell[0]), int(cell[1])))
            else:
                cell = dataSheet.range(cell)
            if type(rowHeight) == float or type(rowHeight) == int:
                if 0 <= rowHeight <= 409.5:
                    cell.row_height = rowHeight
                else:
                    cell.rows.autofit()
                if isSave == 'yes':
                    excel.save()
            else:
                raise Exception("行高错误")
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def setCellColor(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        elif 'color' not in params.keys() or params['color'] is None or params['color'] == '':
            raise Exception('缺少参数：RGB颜色值')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']
            color = eval(params['color'])
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if isinstance(cell, list):
                cell = dataSheet.range((int(cell[0]), int(cell[1])))
            else:
                cell = dataSheet[cell]
            cell.color = (color[0], color[1], color[2])
            if isSave == 'yes':
                excel.save()

    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def setCellFontColor(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        elif 'color' not in params.keys() or params['color'] is None or params['color'] == '':
            raise Exception('缺少参数：RGB颜色值')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # cell = formatCell(params['cell'])
            cell = params['cell']
            color = eval(params['color'])
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if isinstance(color, tuple) or isinstance(color, list):
                color = rgb_to_int(color)
            else:
                color = int(color, 16)
            if type(cell) == list:
                cell = dataSheet.range((int(cell[0]), int(cell[1])))
            else:
                cell = dataSheet[cell]
            cell.api.Font.Color = color
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def setCellFontBold(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            cell = params['cell']
            isSave = params['isSave']
            dataSheet = excel.sheets[excel_sheet]
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            if type(cell) == list:
                cell = dataSheet.range((int(cell[0]), int(cell[1])))
            else:
                cell = dataSheet[cell]
            cell.api.Font.Bold = True
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def setCellFontBorders(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            cell = params['cell']
            isSave = params['isSave']
            dataSheet = excel.sheets[excel_sheet]
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            if type(cell) == list:
                cell = dataSheet.range((int(cell[0]), int(cell[1])))
            else:
                cell = dataSheet[cell]
            # cell.api.Font.Bold = True
            for i in cell:
                i.api.Borders(7).LineStyle = 1
                i.api.Borders(8).LineStyle = 1
                i.api.Borders(9).LineStyle = 1
                i.api.Borders(10).LineStyle = 1
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def setCellFontName(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cell' not in params.keys() or params['cell'] is None or params['cell'] == '':
            raise Exception('缺少参数：指定单元格')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            cell = params['cell']
            isSave = params['isSave']
            dataSheet = excel.sheets[excel_sheet]
            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))
            if type(cell) == list:
                cell = dataSheet.range((int(cell[0]), int(cell[1])))
            else:
                cell = dataSheet[cell]
            cell.api.Font.Name = params['f_name']
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("单元格格式不正确")
    except Exception as e:
        raise e


def formatRange(range):
    if "," in range or "，" in range:
        rangeList = range.replace("，", ",").replace(
            "[", "").replace(']', '').split(",")
        left, right = list(), list()
        for index, r in enumerate(rangeList):
            if index < len(rangeList) / 2:
                left.append('"' + r + '"')
            else:
                right.append('"' + r + '"')
        left = ",".join(left)
        right = ",".join(right)
        range = eval('[' + '[' + left + '],' + '[' + right + ']' + ']')
    return range


def setRangeFontColor(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'range' not in params.keys() or params['range'] is None or params['range'] == '':
            raise Exception('缺少参数：指定范围')
        elif 'color' not in params.keys() or params['color'] is None or params['color'] == '':
            raise Exception('缺少参数：RGB颜色值')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # excel_range = formatRange(params['range'])
            excel_range = params['range']
            color = eval(params['color'])
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if isinstance(color, tuple) or isinstance(color, list):
                color = rgb_to_int(color)
            else:
                color = int(color, 16)
            if type(excel_range) == list:
                excel_range = dataSheet.range((int(excel_range[0][0]), int(excel_range[0][1])),
                                              (int(excel_range[1][0]), int(excel_range[1][1])))
            else:
                excel_range = dataSheet.range(excel_range)
            excel_range.api.Font.Color = color
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("区域格式不正确")
    except Exception as e:
        raise e


def setRangeColor(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'range' not in params.keys() or params['range'] is None or params['range'] == '':
            raise Exception('缺少参数：指定范围')
        elif 'color' not in params.keys() or params['color'] is None or params['color'] == '':
            raise Exception('缺少参数：RGB颜色值')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            # excel_range = formatRange(params['range'])
            excel_range = params['range']
            color = eval(params['color'])
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            if type(excel_range) == list:
                excel_range = dataSheet.range((int(excel_range[0][0]), int(excel_range[0][1])),
                                              (int(excel_range[1][0]), int(excel_range[1][1])))
            else:
                excel_range = dataSheet.range(excel_range)
            if isinstance(color, str):
                color = int(color, 16)
                excel_range.color = color
            else:
                excel_range.color = (color[0], color[1], color[2])
            if isSave == 'yes':
                excel.save()
    except ValueError:
        raise Exception("区域格式不正确")
    except Exception as e:
        raise e


def formatCell(cell):
    if cell.startswith("[") and cell.endswith("]"):
        cell = cell[1:-1]
    if "," in cell or "，" in cell:
        cellList = cell.replace("，", ",").split(",")
        newList = list()
        for c in cellList:
            newList.append('"' + c + '"')
        cell = ",".join(newList)
        cell = eval('[' + cell + ']')
    return cell


def createSheet(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'newSheetName' not in params.keys() or params['newSheetName'] is None or params['newSheetName'] == '':
            raise Exception('缺少参数：新工作表名')
        elif 'sheetName' not in params.keys() or params['sheetName'] is None or params['sheetName'] == '':
            raise Exception('缺少参数：参照工作表名')
        else:
            excel = params['excel_handle']
            sheetName = params['sheetName']
            position = params['position']
            isSave = params['isSave']
            newSheetName = params['newSheetName']

            if sheetIsExists(excel, newSheetName):
                raise Exception("Sheet名字已存在")

            if sheetIsExists(excel, sheetName) is False:
                raise Exception("参照表Sheet名字不存在")

            # dataSheet = excel.sheets.active
            dataSheet = excel.sheets[sheetName]
            if position == 'before':
                excel.sheets.add(name=newSheetName,
                                 before=dataSheet, after=None)
            elif position == 'back':
                excel.sheets.add(name=newSheetName,
                                 after=dataSheet, before=None)
            if isSave == 'yes':
                excel.save()

    except Exception as e:
        raise e


def getSheetsName(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        else:
            excel = params['excel_handle']
            sheetNames = list()
            for s in excel.sheets:
                sheetNames.append(s.name)
            return sheetNames
    except Exception as e:
        raise e


def reNameSheet(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'newSheetName' not in params.keys() or params['newSheetName'] is None or params['newSheetName'] == '':
            raise Exception('缺少参数：新工作表名')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            newSheetName = params['newSheetName']
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            for s in excel.sheets:
                if newSheetName == s.name:
                    raise Exception("工作表名已存在")
            dataSheet = excel.sheets[excel_sheet]
            dataSheet.name = newSheetName
            if isSave == 'yes':
                excel.save()
    except Exception as e:
        raise e


def copySheet(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'newSheetName' not in params.keys() or params['newSheetName'] is None or params['newSheetName'] == '':
            raise Exception('缺少参数：新工作表名')
        else:
            excel = params['excel_handle']
            isSave = params['isSave']
            excel_sheet = params['sheet']
            newSheetName = params['newSheetName']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            dataSheet = excel.sheets[excel_sheet]
            dataSheet.api.Copy(Before=(dataSheet.api))
            newSheet = excel.sheets[(dataSheet.index - 2)]
            if newSheetName != '':
                for sheet in excel.sheets:
                    if sheet.name == newSheetName:
                        raise Exception("工作表名字已存在")

                newSheet.name = newSheetName
            if isSave == 'yes':
                excel.save()
            return newSheet.name
    except Exception as e:
        raise e


def deleteSheet(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            isSave = params['isSave']

            if not sheetIsExists(excel, excel_sheet):
                raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

            sheet = excel.sheets[excel_sheet]
            sheet.delete()
            if isSave == 'yes':
                excel.save()
    except Exception as e:
        raise e


def excuteMacro(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'define' not in params.keys() or params['define'] is None or params['define'] == '':
            raise Exception('缺少参数：宏定义')
        elif 'parameters' not in params.keys() or params['parameters'] is None:
            raise Exception('缺少参数：宏参数')
        else:
            excel = params['excel_handle']
            define = params['define']
            parameters = params['parameters']
            result = None

            macro = excel.macro(define)

            if parameters == '':
                result = eval('macro()')
            else:
                result = eval('macro(%s)' % parameters)

            return result
    except Exception as e:
        raise e


def read_cols_rows(params):
    try:
        if 'excel_handle' not in params.keys() or params['excel_handle'] is None or params['excel_handle'] == '':
            raise Exception('缺少参数：excel句柄')
        elif 'sheet' not in params.keys() or params['sheet'] is None or params['sheet'] == '':
            raise Exception('缺少参数：工作表名称')
        elif 'cols' not in params.keys() or params['cols'] is None or params['cols'] == '':
            raise Exception('缺少参数：列名称')
        else:
            excel = params['excel_handle']
            excel_sheet = params['sheet']
            cols = params['cols']

        if not sheetIsExists(excel, excel_sheet):
            raise Exception('%s 文件中不存在 %s 工作表' % (excel.name, excel_sheet))

        data = list()
        dataSheet = excel.sheets[excel_sheet]
        for i in range(int(cols[0][0]), 10):
            temp = dataSheet.range((i, int(cols[0][1])), (i, int(cols[1][1]))).value
            if temp:
                if temp[0] is None or temp[0] == '':
                    if temp[1] is None or temp[1] == '':
                        break
                data.append(temp)
        if data:
            if type(data[0]) != list:
                data = [data]
        return data
    except Exception as e:
        raise e


def write_excel(params):
    if 'data' not in params.keys() or params['data'] is None:
        raise Exception("缺少参数：行数据")
    elif not isinstance(params['data'], list):
        raise Exception("行数据类型必须为数组")
    elif 'excel_path' not in params.keys() or params['excel_path'] is None or params['excel_path'] == '':
        raise Exception("缺少参数：excel保存位置")
    elif 'file_name' not in params.keys() or params['file_name'] is None or params['file_name'] == '':
        raise Exception("缺少参数：保存文件名")
    elif 'write_mode' not in params.keys() or params['write_mode'] is None or params['write_mode'] == '':
        raise Exception("缺少参数：写入模式")
    else:
        file_name = params['file_name']
        file_path = os.path.join(params['excel_path'], file_name)
        if params['write_mode'] == 'append':
            if not os.path.exists(file_path):
                wb = Workbook()
            else:
                wb = load_workbook(file_path)
        elif params['write_mode'] == 'cover':
            wb = Workbook()
        else:
            raise Exception('尚未支持该写入模式')
        data_list = params['data']
        ws = wb.active
        for data in data_list:
            ws.append(data)
        wb.save(file_path)
        return file_path


def excel_dir_concat(params):
    if 'suffix' not in params.keys() or params['suffix'] is None:
        raise Exception("缺少参数：后缀")
    elif 'excel_dir' not in params.keys() or params['excel_dir'] is None or params['excel_dir'] == '':
        raise Exception("缺少参数：保存位置")
    elif 'out_filename' not in params.keys() or params['out_filename'] is None or params['out_filename'] == '':
        raise Exception("缺少参数：保存文件名")
    else:
        suffix = params['suffix']  # 需要合并的后缀
        excel_dir = params['excel_dir']  # 合并目录
        out_filename = params['out_filename']  # 输出文件名
        excel_name_list = [i for i in os.listdir(excel_dir) if i.split('.')[-1] == suffix]  # 获取相同后缀的文件
        df_list = []
        for filename in excel_name_list:
            excel_path = os.path.join(excel_dir, filename)
            df_list.append(pd.read_excel(excel_path, dtype='str'))
        df = pd.concat(df_list)
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        df.to_excel(os.path.join(excel_dir, out_filename))
        return "Excel合并成功"


def dict_to_excel(params):
    if 'data' not in params.keys() or params['data'] is None:
        raise Exception("缺少参数：行数据")
    elif not isinstance(eval(params['data']), list):
        raise Exception("行数据类型必须为数组")
    elif 'excel_path' not in params.keys() or params['excel_path'] is None or params['excel_path'] == '':
        raise Exception("缺少参数：excel保存位置")
    elif 'file_name' not in params.keys() or params['file_name'] is None or params['file_name'] == '':
        raise Exception("缺少参数：保存文件名")
    else:
        data = eval(params['data'])
        file_name = params['file_name']
        file_path = os.path.join(params['excel_path'], file_name)
        pd.DataFrame(data, dtype='str').to_excel(file_path, index=False)


def read_Excel(params):
    try:
        if 'excel_path' not in params.keys() or params['excel_path'] is None or params['excel_path'] == '':
            raise Exception('缺少参数：excel路径地址')
        elif not os.path.exists(params['excel_path']):
            raise Exception('Excel文件不存在！！！')
        else:
            excel = params['excel_path']
            workbook = xlrd.open_workbook(excel)
            data = []
            sheet = workbook.sheet_by_index(0)
            try:
                if 'sheet' not in params.keys() or params['sheet'] == '' or params['sheet'] is None:
                    sheet = workbook.sheet_by_index(0)
                elif type(params['sheet']) == str:
                    sheet = workbook.sheet_by_name(params['sheet'])
                elif type(params['sheet']) == int or params['sheet'].isdigit():
                    index = int(params['sheet'])
                    sheet = workbook.sheet_by_index(index)
                else:
                    pass
            except Exception as e:
                print('sheet参数错误！')
                raise e
            nrows = sheet.nrows
            print(nrows)
            for row in range(nrows):
                values = sheet.row_values(row)
                data.append(values)
        return data
    except Exception as e:
        raise e


def data_to_excel(params):
    try:
        if 'headers' not in params.keys() or params['headers'] is None or params['headers'] == '':
            raise Exception('缺少参数：标题行')
        elif 'data' not in params.keys() or params['data'] is None or params['data'] == '':
            raise Exception('缺少参数：数据行')
        elif 'path' not in params.keys() or params['path'] is None or params['path'] == '':
            raise Exception('缺少参数：文件路径')
        elif 'file_name' not in params.keys() or params['file_name'] is None or params['file_name'] == '':
            raise Exception('缺少参数：保存的文件名')
        elif 'write_type' not in params.keys() or params['write_type'] is None or params['write_type'] == '':
            raise Exception('缺少参数：写入类型')
        else:
            headers_list = params['headers']
            data_list = params['data']
            if type(headers_list) != list or type(data_list) != list:
                raise Exception("参数标题行或数据行类型错误，应为list类型")
            excel_path = os.path.join(params['path'], params['file_name'])
            if '.xls' not in params['file_name']:
                file_name = params['file_name'] + '.xlsx'
                excel_path = os.path.join(params['path'], file_name)
            # print(excel_path)
            if os.path.exists(excel_path):
                wb = load_workbook(excel_path)
                sheet = wb.active
                nrows = sheet.max_row
                if params['write_type'] == 'append':
                    try:
                        if len(headers_list) == len(data_list):
                            for n in range(len(headers_list)):

                                for i in range(len(headers_list[n])):
                                    sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                for i in range(len(data_list[n])):
                                    sheet.cell(row=nrows + 2, column=i + 1, value=data_list[n][i])
                                nrows += 2
                        elif len(headers_list) < len(data_list):
                            for n in range(len(data_list)):
                                if n >= len(headers_list):
                                    for i in range(len(data_list[n])):
                                        sheet.cell(row=nrows + 1, column=i + 1, value=data_list[n][i])
                                    nrows += 1
                                else:
                                    for i in range(len(headers_list[n])):
                                        sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                    for i in range(len(data_list[n])):
                                        sheet.cell(row=nrows + 2, column=i + 1, value=data_list[n][i])
                                    nrows += 2
                        else:
                            for n in range(len(headers_list)):
                                if n >= len(data_list):
                                    for i in range(len(headers_list[n])):
                                        sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                    nrows += 1
                                else:
                                    for i in range(len(headers_list[n])):
                                        sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                    for i in range(len(data_list[n])):
                                        sheet.cell(row=nrows + 2, column=i + 1, value=data_list[n][i])
                                    nrows += 2
                    except Exception as e:
                        print(e)
                if params['write_type'] == 'cover':
                    try:
                        nrows = 0
                        if len(headers_list) == len(data_list):
                            for n in range(len(headers_list)):
                                for i in range(len(headers_list[n])):
                                    sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                for i in range(len(data_list[n])):
                                    sheet.cell(row=nrows + 2, column=i + 1, value=data_list[n][i])
                                nrows += 2
                        elif len(headers_list) < len(data_list):
                            for n in range(len(data_list)):
                                if n >= len(headers_list):
                                    for i in range(len(data_list[n])):
                                        sheet.cell(row=nrows + 1, column=i + 1, value=data_list[n][i])
                                    nrows += 1
                                else:
                                    for i in range(len(headers_list[n])):
                                        sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                    for i in range(len(data_list[n])):
                                        sheet.cell(row=nrows + 2, column=i + 1, value=data_list[n][i])
                                    nrows += 2
                        else:
                            for n in range(len(headers_list)):
                                if n >= len(data_list):
                                    for i in range(len(headers_list[n])):
                                        sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                    nrows += 1
                                else:
                                    for i in range(len(headers_list[n])):
                                        sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                    for i in range(len(data_list[n])):
                                        sheet.cell(row=nrows + 2, column=i + 1, value=data_list[n][i])
                                    nrows += 2
                    except Exception as e:
                        print(e)
                wb.save(excel_path)
            else:
                wb = Workbook()
                sheet = wb.active
                nrows = 0
                try:
                    if len(headers_list) == len(data_list):
                        for n in range(len(headers_list)):
                            for i in range(len(headers_list[n])):
                                sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                            for i in range(len(data_list[n])):
                                sheet.cell(row=nrows + 2, column=i + 1, value=data_list[n][i])
                            nrows += 2
                    elif len(headers_list) < len(data_list):
                        for n in range(len(data_list)):
                            if n >= len(headers_list):
                                for i in range(len(data_list[n])):
                                    sheet.cell(row=nrows + 1, column=i + 1, value=data_list[n][i])
                                nrows += 1
                            else:
                                for i in range(len(headers_list[n])):
                                    sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                for i in range(len(data_list[n])):
                                    sheet.cell(row=nrows + 2, column=i + 1, value=data_list[n][i])
                                nrows += 2
                    else:
                        for n in range(len(headers_list)):
                            if n >= len(data_list):
                                for i in range(len(headers_list[n])):
                                    sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                nrows += 1
                            else:
                                for i in range(len(headers_list[n])):
                                    sheet.cell(row=nrows + 1, column=i + 1, value=headers_list[n][i])
                                for i in range(len(data_list[n])):
                                    sheet.cell(row=nrows + 2, column=i + 1, value=data_list[n][i])
                                nrows += 2
                except Exception as e:
                    print(e)
                wb.save(excel_path)
        return excel_path
    except Exception as e:
        raise e


if __name__ == '__main__':
    params ={"path": "C:\\Users\\15842\\Desktop\\test.xlsx", "visible": "yes"}
    excel_handle = open_excel(params)
