import os
import bs4
import copy
import datetime
import smtplib
from email.mime.text import MIMEText
from email.header import Header, decode_header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import poplib
from email.message import Message  # 邮件对象
from email.parser import Parser  # 解析模块
from email.utils import parseaddr, formataddr  # 用于格式化邮件信息


class MailMessage(object):
    def __init__(self, msg: Message):
        self.message = msg

    @property
    def recipient(self):
        """
        解析收件人：名+地址
        """
        data = self.message.get("To", "")
        recipient = self._format_addr(data)
        return recipient

    @property
    def sender(self):
        """
        解析发件人：名+地址
        """
        data = self.message.get("From", "")
        sender = self._format_addr(data)
        return sender

    @property
    def subject(self):
        """
        解析主题
        """
        data = self.message.get("Subject", "")
        # print(data, type(data))
        # print(parseaddr(data))
        # subject = self._decode_str(parseaddr(data))
        subject = self._decode_str(data)
        return subject

    @property
    def date(self):
        """
        解析发件时间
        """
        data = self.message.get("Date", "")[:25]
        format = "%a, %d %b %Y %H:%M:%S"
        if data == "":
            return ""
        date = datetime.datetime.strptime(data, format)
        return date.strftime("%Y-%m-%d %H:%M:%S")

    @property
    def content(self):
        """
        解析邮件内容
        """
        content = ""
        for part in self.message.walk():
            content_type = part.get_content_type()
            # print(content_type)
            charset = self._guess_charset(part)
            # 如果有附件，则直接跳过
            if part.get_filename() is not None:
                continue
            email_content_type = ''
            if charset:
                if 'utf-8' in charset:
                    charset = 'utf-8'
                if content_type == 'text/plain':
                    email_content_type = 'text'
                    content = part.get_payload(decode=True).decode(charset, errors='ignore')
                elif content_type == 'text/html':
                    email_content_type = 'html'
                    content = part.get_payload(decode=True).decode(charset, errors='ignore')
                    bs = bs4.BeautifulSoup(content, "html.parser")
                    [b.extract() for b in bs(['script', 'style'])]
                    content = bs.get_text().lstrip().rstrip().encode(charset)  # .decode()
                    import chardet
                    charset = chardet.detect(content)['encoding']
                    print("编码：", chardet.detect(content))
                    if charset == 'ascii':
                        charset = 'unicode_escape'
                    content = content.decode(charset)
                    #elif charset != None:
                    #    content = content.decode(charset)
                    #else:
                    #    content = content.decode()
            if email_content_type == '':
                continue
        return content


    def saveFile(self, path):
        """
        保存附件：
            返回False：没有附件
            返回True：有附件并保存成功
        """
        result = False
        for part in self.message.walk():
            file_name = part.get_filename()
            if file_name:
                fileName = Header(file_name)
                fileName = decode_header(fileName)  # 对附件名称进行解码
                filename = fileName[0][0]
                if fileName[0][1]:
                    filename = self._decode_str(str(filename, fileName[0][1]))  # 将附件名称可读化
                data = part.get_payload(decode=True)  # 下载附件
                with open(path + os.path.sep + filename, 'wb') as att_file:  # 在指定目录下创建文件，注意二进制文件需要用wb模式打开
                    att_file.write(data)  # 保存附件
                result = True
        return result

    def toDict(self):
        data = {
            "recipient": self.recipient,
            "sender": self.sender,
            "subject": self.subject,
            "date": self.date,
            "content": self.content
        }
        return data

    def _decode_str(self, data):
        """
        解码函数
        """
        value, charset = decode_header(data)[0]
        if charset:
            value = value.decode(charset, errors='ignore')  # 如果文本中存在编码信息，则进行相应的解码
        return value

    def _guess_charset(self, message=None):
        """
        猜测邮件编码格式
        """
        if message is None:
            message = self.message
        charset = message.get_charset()
        if charset is None:
            content_type = message.get('Content-Type', '').lower()
            pos = content_type.find('charset=')
            if pos >= 0:
                charset = content_type[pos + 8:].strip()
        return charset

    def _format_addr(self, data):
        """
        格式化人+地址
        """
        temp_name, addr = parseaddr(data)
        name = self._decode_str(temp_name)
        return u'%s | <%s>' % (name, addr)


def connect_mailBox(params):
    """
    连接邮箱
    """
    port = int(params['port'])
    port_ssl = int(params['port_ssl'])
    ssl = params['ssl']
    server = ''
    if params['server'] == 'qq':
        server = "pop.qq.com"
    elif params['server'] == 'gmail':
        server = 'pop.gmail.com'
    elif params['server'] == '126':
        server = "pop.126.com"
    elif params['server'] == '163':
        server = 'pop.163.com'

    account = params['account']
    password = params['password']

    popObj = None
    try:
        if ssl == 'no':
            popObj = poplib.POP3(server, port=port if ssl == 'no' else port_ssl)
        elif ssl == 'yes':
            popObj = poplib.POP3_SSL(server, port=port if ssl == 'no' else port_ssl)

        popObj.user(account)
        popObj.pass_(password)
        return popObj
    except Exception as e:
        raise e


def sendMail(params):
    """
    发送邮件
    """
    try:
        to = str(params['toMail'])
        title = str(params['title'])
        content = str(params['content'])
        port = int(params['port'])
        port_ssl = int(params['port_ssl'])
        ssl = params['ssl']
        # addresser = params['addresser']
        server = ''
        if params['server'] == 'qq':
            server = "smtp.qq.com"
        elif params['server'] == 'gmail':
            server = 'smtp.gmail.com'
        elif params['server'] == '126':
            server = "smtp.126.com"
        elif params['server'] == '163':
            server = 'smtp.163.com'

        # if addresser == 'customize':
        account = str(params['account'])
        password = str(params['password'])
        
        # message = MIMEText(content, 'plain', 'utf-8')
        message = MIMEText(content, 'html', 'utf-8')
        m = MIMEMultipart()
        m.attach(message)
        if params['file'] != "":
            if bool(params['reset']) is False:
                mailFile = params['file']
                *_, filename = os.path.split(mailFile)
                sendfile = MIMEApplication(open(mailFile, 'rb').read())
                sendfile.add_header('Content-Disposition', 'attachment', filename=filename)
                m.attach(sendfile)

        if params['files'] != '':
            if bool(params['resets']) is False:
                filespath = params['files']
                if os.path.exists(filespath):
                    for file in os.listdir(filespath):
                        if os.path.isfile(filespath + os.path.sep + file):
                            sendfiles = MIMEApplication(open(filespath + os.path.sep + file, 'rb').read())
                            sendfiles.add_header('Content-Disposition', 'attachment', filename=file)
                            m.attach(sendfiles)
                else:
                    raise RuntimeError(f'路径不存在:{filespath}')


        m['From'] = _format_addr(account.split('@')[0] + '<%s>' % account)
        m['To'] = _format_addr(to.split('@')[0] + '<%s>' % to)
        m['Subject'] = Header(title, 'utf-8').encode()

        smtpObj = None
        if 'no' == ssl:
            smtpObj = smtplib.SMTP()
        elif 'yes' == ssl:
            smtpObj = smtplib.SMTP_SSL(host=server)

        smtpObj.connect(server, port=port if ssl == 'no' else port_ssl)
        smtpObj.login(account, password)
        smtpObj.sendmail(account, [to], m.as_string())
        smtpObj.quit()
    except Exception as e:
        raise e
    return "邮件发送成功"


def getMail(params):
    """
    按类型获取邮件信息：
        收件人：recipient
        主题：subject
        内容：content
        日期：date
        全部：all 【默认】
    """
    email_num = params['email_num']
    pop_obj = connect_mailBox(params)
    message_type = params['type']
    print(message_type)
    try:
        # 根据邮件序号获取邮件信息
        _, mails, a = pop_obj.list()
        count = len(mails)
        if email_num > count:
            return "邮件序号错误"
        # 获取第email_num封邮件
        response, lines, octets = pop_obj.retr(count - email_num + 1)

        msg_content = b'\r\n'.join(lines).decode('utf-8')
        msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
        mmsg = MailMessage(msg)
        message_data = mmsg.toDict()
        copy_message_data = copy.deepcopy(message_data)
        message_data['all'] = copy_message_data
        close_connect(pop_obj)
        return message_data.get(message_type, "获取信息类型错误")
    except Exception as e:
        close_connect(pop_obj)
        raise e


def getMailBy(params):
    """
    筛选邮件
    """
    recipient = params.get('recipient', "")  # 收件人
    sender = params.get('send', "")  # 发件人
    subject = params.get('subject', "")  # 主题
    pop_obj = connect_mailBox(params)  # 邮箱对象
    mails = pop_obj.list()[1]
    mail_list = []
    for i in range(1, len(mails) + 1):
        content = pop_obj.retr(i)[1]
        try:
            msg_content = b'\r\n'.join(content).decode('utf-8')
        except UnicodeDecodeError:
            msg_content = b'\r\n'.join(content).decode('gbk')
        except Exception as e:
            raise e
        msg = Parser().parsestr(msg_content)
        mmsg = MailMessage(msg)
        m_recipient = mmsg.recipient
        m_sender = mmsg.sender
        m_subject = mmsg.subject
        if (recipient != "" and recipient in m_recipient) or (sender != "" and sender in m_sender) or (subject != "" and subject in m_subject):
            mail_list.append(mmsg.toDict())
    close_connect(pop_obj)
    return mail_list


def getFile(params):
    """
    获取附件
        返回False：没有附件或保存失败
        返回True：有附件并保存成功
    """
    num = int(params['email_num'])
    path = params['path']
    popObj = connect_mailBox(params)

    try:
        resp, mails, octets = popObj.list()
        count = len(mails)
        if num > count:
            return "邮件序号错误"
        resp, lines, octets = popObj.retr(count - num + 1)
        msg_content = b'\r\n'.join(lines).decode('utf-8')
        msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
        mmsg = MailMessage(msg)

        result = mmsg.saveFile(path)
        close_connect(popObj)
        return result
    except Exception as e:
        raise e


def close_connect(popObj):
    """
    关闭邮箱连接
    """
    try:
        popObj.close()
        return "关闭成功"
    except IndexError:
        return ""
    except Exception:
        return "关闭失败"


# 格式化用户名和邮件地址
def _format_addr(s):
    name, addr = parseaddr(s)
    return formataddr((Header(name, 'utf-8').encode(), addr))  # .encode('utf-8') if isinstance(addr, str) else addr))






if __name__ == '__main__':

    pass