import win32con
import win32process
import psutil
from psutil import NoSuchProcess
import win32api
import win32event
import pywintypes
from win32com.shell.shell import ShellExecuteEx
from win32com.shell import shellcon
import autoit
import traceback
import struct
import win32clipboard
from io import BytesIO
from PIL import Image
import os
import time
import json
import tempfile
import pygame
from pymediainfo import MediaInfo
import shutil
from os.path import expanduser
import win32com.client
import tkinter as tk
import tkinter.messagebox
import tkinter.filedialog


def openApp(params):
    try:
        # from win32com.shell.shell import ShellExecuteEx
        # from win32com.shell import shellcon
        path = params['path']
        style = params["style"]
        wait = params["wait"]
        # flags = 0
        # showType = 0
        if style == 'default':
            showType = win32con.SW_NORMAL
            showType = win32con.SW_SHOWNORMAL
        if style == 'max':
            showType = win32con.SW_MAXIMIZE
            showType = win32con.SW_SHOWMAXIMIZED
        if style == 'min':
            showType = win32con.SW_MINIMIZE
            showType = win32con.SW_SHOWMINIMIZED
        if style == 'hide':
            showType = win32con.SW_HIDE
        # if showType == win32con.SW_HIDE:
        #     flags = flags | win32process.CREATE_NO_WINDOW
        # info = win32process.GetStartupInfo()
        # flags = flags | win32process.CREATE_NEW_CONSOLE
        # info.dwFlags = info.dwFlags | win32process.STARTF_USESHOWWINDOW | win32process.STARTF_USESTDHANDLES
        # info.wShowWindow = info.wShowWindow | showType
        # hProcess, hThread, processId, _ = win32process.CreateProcess(
        #     None, path, None, None, False, flags, None, os.path.expanduser('~'), info)
        process_info = ShellExecuteEx(fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                                      hwnd=None, lpVerb='runas', lpFile=path, lpDirectory='', nShow=showType)
        if 'hProcess' in process_info:
            hProcess = process_info['hProcess']
        if 'hInstApp' in process_info:
            hInstApp = process_info['hInstApp']
        Success = False
        if hInstApp > 32:
            Success = True
        if Success:
            if not hProcess:
                return 0
        else:
            return 0
        processId = win32process.GetProcessId(hProcess)
        if wait == 'no':
            # win32api.CloseHandle(hThread)
            win32api.CloseHandle(hProcess)
            return processId
        elif wait == "ready":
            processId = win32process.GetProcessId(hProcess)
            win32event.WaitForInputIdle(hProcess, win32event.INFINITE)
            win32api.CloseHandle(hProcess)
            # win32api.CloseHandle(hThread)
            return processId
        elif wait == "exec":
            win32event.WaitForSingleObject(hProcess, win32event.INFINITE)
            ecode = win32process.GetExitCodeProcess(hProcess)
            win32api.CloseHandle(hProcess)
            # win32api.CloseHandle(hThread)
            return ecode
    except pywintypes.error:
        return processId
    except Exception as e:
        raise e


def openFile(params):
    try:
        # from win32com.shell.shell import ShellExecuteEx
        # from win32com.shell import shellcon
        path = None
        otype = params["type"]
        if otype == "site":
            if params['site'] != '':
                if "http://" in params['site'] or "https://" in params['site']:
                    path = params["site"]
                else:
                    path = "http://" + params["site"]
        if otype == "file":
            if params["path"] != "":
                path = params["path"]
        style = params["style"]
        wait = params["wait"]
        hProcess = None
        hInstApp = None
        if style == 'default':
            style = win32con.SW_NORMAL
            style = win32con.SW_SHOWNORMAL
        elif style == 'max':
            style = win32con.SW_MAXIMIZE
            style = win32con.SW_SHOWMAXIMIZED
        elif style == 'min':
            style = win32con.SW_MINIMIZE
            style = win32con.SW_SHOWMINIMIZED
        elif style == 'hide':
            style = win32con.SW_HIDE
        process_info = ShellExecuteEx(fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                                      hwnd=None, lpVerb='open', lpFile=path, lpDirectory='', nShow=style)
        if 'hProcess' in process_info:
            hProcess = process_info['hProcess']
        if 'hInstApp' in process_info:
            hInstApp = process_info['hInstApp']
        Success = False
        if hInstApp > 32:
            Success = True
        if Success:
            if not hProcess:
                return 0
        else:
            return 0
        if wait == 'no':
            pid = win32process.GetProcessId(hProcess)
            win32api.CloseHandle(hProcess)
            return pid
        elif wait == 'ready':
            win32event.WaitForInputIdle(hProcess, win32event.INFINITE)
            pid = win32process.GetProcessId(hProcess)
            win32api.CloseHandle(hProcess)
            return pid
        elif wait == "exec":
            win32event.WaitForSingleObject(hProcess, win32event.INFINITE)
            ecode = win32process.GetExitCodeProcess(hProcess)
            win32api.CloseHandle(hProcess)
            return ecode
    except pywintypes.error:
        return 0
    except Exception as e:
        raise e


def closeApp(params):
    try:
        if params['ptype'] == 'pid':
            pro = psutil.Process(int(params['pid']))
            pro.kill()
        elif params['ptype'] == 'pname':
            pids = psutil.pids()
            for pid in pids:
                pro = psutil.Process(pid)
                if params['pname'] == pro.name():
                    pro.kill()
    except Exception as e:
        raise e


def getAppStatus(params):
    try:
        if params['ptype'] == 'pid':
            pro = psutil.Process(int(params['pid']))
            if pro.status() == 'running':
                return True
        elif params['ptype'] == 'pname':
            pids = psutil.pids()
            pList = list()
            for pid in pids:
                pro = psutil.Process(pid)
                pList.append(pro.name())
            if params['pname'] in pList:
                return True
        return False
    except NoSuchProcess:
        return False
    except Exception as e:
        raise e


# 装饰器用来捕获异常
def easy_try_except(fun):
    def add_cap(*args):
        try:
            result = fun(*args)
            return result
        except:
            traceback.print_exc()
            return

    return add_cap


# @easy_try_except
def start_client(params):
    if 'exe_path' not in params.keys() or params['exe_path'] is None or params['exe_path'] == '':
        raise Exception('缺少参数')
    try:
        win32api.ShellExecute(0, 'open', params['exe_path'], '', '', 1)
    except Exception as e:
        raise e
    return


# @easy_try_except
def control_click(params):
    if 'title_or_class' not in params.keys() or params['title_or_class'] is None or params['title_or_class'] == '':
        raise Exception('缺少参数')
    try:
        if params['title_or_class'] == 'class':
            class_ = '[CLASS:{}]'.format(params['class'])
            autoit.control_click(class_, params['class_name_nn'])
        else:
            autoit.control_click(params['title'], params['class_name_nn'])
    except Exception as e:
        raise e
    return


# @easy_try_except
def control_set_text(params):
    if 'title_or_class' not in params.keys() or params['title_or_class'] is None or params['title_or_class'] == '':
        raise Exception('缺少参数')
    try:
        if params['title_or_class'] == 'class':
            class_ = '[CLASS:{}]'.format(params['class'])
        else:
            class_ = params['title']
        if params['clear']:
            autoit.control_set_text(class_, params['class_name_nn'], '')
        autoit.control_set_text(class_, params['class_name_nn'], params['input_text'])
    except Exception as e:
        raise e
    return


# @easy_try_except
def win_activate(params):
    if 'title_or_class' not in params.keys() or params['title_or_class'] is None or params['title_or_class'] == '':
        raise Exception('缺少参数')
    try:
        if params['title_or_class'] == 'class':
            class_ = '[CLASS:{}]'.format(params['class'])
        else:
            class_ = params['title']
        autoit.win_activate(class_)
    except Exception as e:
        raise e
    return


# @easy_try_except
def win_set_on_top(params):
    if 'title_or_class' not in params.keys() or params['title_or_class'] is None or params['title_or_class'] == '':
        raise Exception('缺少参数')
    try:
        if params['title_or_class'] == 'class':
            class_ = '[CLASS:{}]'.format(params['class'])
        else:
            class_ = params['title']
        autoit.win_set_on_top(class_)
    except Exception as e:
        raise e
    return


# @easy_try_except
def win_get_text(params):
    if 'title_or_class' not in params.keys() or params['title_or_class'] is None or params['title_or_class'] == '':
        raise Exception('缺少参数')
    try:
        if params['title_or_class'] == 'class':
            class_ = '[CLASS:{}]'.format(params['class'])
        else:
            class_ = params['title']
        return autoit.win_get_text(class_)
    except Exception as e:
        raise e


# @easy_try_except
def close_client(params):
    if 'title_or_class' not in params.keys() or params['title_or_class'] is None or params['title_or_class'] == '':
        raise Exception('缺少参数')
    try:
        if params['title_or_class'] == 'class':
            class_ = '[CLASS:{}]'.format(params['class'])
        else:
            class_ = params['title']
        autoit.win_close(class_)
    except Exception as e:
        raise e
    return


def setText(params):
    if 'text' not in params.keys() or params['text'] is None or params['text'] == '':
        raise Exception('缺少参数')
    try:
        text = str(params['text'])
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        win32clipboard.SetClipboardData(win32con.CF_UNICODETEXT, str(text))
    except Exception as e:
        raise e
    finally:
        win32clipboard.CloseClipboard()
    return text


def getText(params):
    try:
        win32clipboard.OpenClipboard()
        text = win32clipboard.GetClipboardData(win32con.CF_UNICODETEXT)
    except Exception as e:
        raise e
    finally:
        win32clipboard.CloseClipboard()
    return text


def setImage(params):
    if 'path' not in params.keys() or params['path'] is None or params['path'] == '':
        raise Exception('缺少参数')
    try:
        path = params['path']
        filepath, filename = os.path.split(path)
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        img = Image.open(path)
        output = BytesIO()
        img.convert('RGB').save(output, "BMP")
        data = output.getvalue()[14:]
        output.close()
        win32clipboard.SetClipboardData(win32con.CF_DIB, data)
        return filename
    except Exception as e:
        raise e
    finally:
        win32clipboard.CloseClipboard()


def getImage(params):
    if 'path' not in params.keys() or params['path'] is None or params['path'] == '':
        raise Exception('缺少参数')
    try:
        path = params['path']
        filename = params['name']
        win32clipboard.OpenClipboard()
        if win32clipboard.IsClipboardFormatAvailable(win32clipboard.CF_DIB):
            sBitmap = win32clipboard.GetClipboardData(win32clipboard.CF_DIB)
            # 构建 BMP 文件头 [ BM + (int)Bitmap数据长度 + (int)Reserved + (int)OffBits ]
            sBmpHdr = struct.pack('<HIII', 0x4d42, len(sBitmap), 0, 54)
            with open(path + "\\" + filename, 'wb') as sFile:
                sFile.write(sBmpHdr)
                sFile.write(sBitmap)
    except Exception as e:
        raise e
    finally:
        win32clipboard.CloseClipboard()


def MsgBox(params):
    if 'message' not in params.keys() or params['message'] is None or params['message'] == '':
        raise Exception('缺少参数')
    elif 'title' not in params.keys() or params['title'] is None or params['title'] == '':
        raise Exception('缺少参数')
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        root.withdraw()
        # tk.Tk().wm_attributes('-topmost', 1)
        # tk.Tk().withdraw()
        message = params['message']
        title = params['title']
        button = params['button']
        icon = params['icon']
        msg = tkinter.messagebox.showinfo(title=title, message=message, icon=icon, type=button)
        return msg
    except Exception as e:
        raise e


def InputBox(params):
    if 'message' not in params.keys() or params['message'] is None or params['message'] == '':
        raise Exception('缺少参数')
    elif 'title' not in params.keys() or params['title'] is None or params['title'] == '':
        raise Exception('缺少参数')
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        message = params['message']
        title = params['title']
        default = params['default']
        # onlyNum = params['number']
        root.title(title)
        string = tk.StringVar()
        string.set(default)
        label = tk.Label(root, text=message, padx=20, pady=10)
        label.grid(row=0, column=0, sticky=tk.NW, columnspan=2)
        text = tk.Entry(root, textvariable=string, width=55)
        text.grid(row=2, column=0, sticky=tk.NW, padx=20, columnspan=2, pady=5)
        bt1 = tk.Button(root, text="确定", width=10, command=root.destroy)
        bt2 = tk.Button(root, text="取消", width=10, command=lambda: cancel(string, root))
        bt1.grid(row=3, column=0, sticky=tk.SW, padx=50, pady=5, columnspan=2)
        bt2.grid(row=3, column=1, sticky=tk.SE, padx=50, pady=5, columnspan=2)
        root.mainloop()

        return string.get()
    except Exception as e:
        raise e


def cancel(string, root):
    string.set("")
    root.destroy()


def FileBox(params):
    if 'path' not in params.keys() or params['path'] is None or params['path'] == '':
        raise Exception('缺少参数')
    elif 'title' not in params.keys() or params['title'] is None or params['title'] == '':
        raise Exception('缺少参数')
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        path = params['path']
        title = params['title']
        filterFile = params['filter']
        multiple = False if params['multiple'] == 'no' else True
        root.withdraw()
        if not multiple:
            temp = list()
            temp.append(str(tk.filedialog.askopenfilename(initialdir=path, title=title, filetypes=filterFile,
                                                          multiple=multiple)))
            return temp[0]
        else:
            temp = list(
                tk.filedialog.askopenfilename(initialdir=path, title=title, filetypes=filterFile, multiple=multiple))
            return temp
    except Exception as e:
        raise e


def SaveFileBox(params):
    if 'path' not in params.keys() or params['path'] is None or params['path'] == '':
        raise Exception('缺少参数')
    elif 'title' not in params.keys() or params['title'] is None or params['title'] == '':
        raise Exception('缺少参数')
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        path = params['path']
        title = params['title']
        filterFile = params['filter']
        root.withdraw()
        return tk.filedialog.asksaveasfile(initialdir=path, title=title, filetypes=filterFile,
                                           defaultextension='txt')
    except Exception as e:
        raise e


def NotifyBox(params):
    if 'message' not in params.keys() or params['message'] is None or params['message'] == '':
        raise Exception('缺少参数')
    elif 'title' not in params.keys() or params['title'] is None or params['title'] == '':
        raise Exception('缺少参数')
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        root.withdraw()
        message = params['message']
        title = params['title']
        icon = params['icon']
        return eval("tkinter.messagebox.show" + icon + "(title=title, message=message, icon=icon, type='ok')")
    except Exception as e:
        raise e


def playSound(params):
    if 'path' not in params.keys() or params['path'] is None or params['path'] == '':
        raise Exception('缺少参数')
    try:
        path = params["path"]
        pygame.mixer.init()
        pygame.mixer.music.load(path)

        music = MediaInfo.parse(path).to_json()
        music = json.loads(music)
        length = music['tracks'][0]['duration'] / 1000

        while True:
            pygame.mixer.music.play(loops=0, start=0.0)
            pygame.mixer.music.set_volume(0.5)
            time.sleep(length + 1)
            pygame.mixer.music.stop()
            break
    except Exception as e:
        raise e


def fileIsExist(params):
    if 'path' not in params.keys() or params['path'] is None or params['path'] == '':
        raise Exception('缺少参数')
    path = params['path']
    data = False
    try:
        if os.path.exists(path):
            data = True
    except Exception as e:
        raise e
    return data


def getEnv(params):
    if 'env' not in params.keys() or params['env'] is None or params['env'] == '':
        raise Exception('缺少参数')
    try:
        env = params['env']
        if env == '':
            return dict(os.environ)
        else:
            return os.getenv(env)
    except KeyError:
        return "没有该环境变量"
    except Exception as e:
        raise e


def setEnv(params):
    if 'env' not in params.keys() or params['env'] is None or params['env'] == '':
        raise Exception('缺少参数')
    elif 'value' not in params.keys() or params['value'] is None or params['value'] == '':
        raise Exception('缺少参数')
    try:
        env = params["env"]
        value = params["value"]
        if params['isForever'] == 'no':
            os.environ[env] = value
        else:
            cmd = "setx %s %s" % (env, value)
            params['cmd'] = cmd
            os.system(cmd)
            # execPowerShell(params)
            # execCommand(params['cmd'])
    except Exception as e:
        raise e


def execCommand(params):
    try:
        cmd = params["cmd"]
        result = os.popen(cmd, 'r')
        return result.read()
    except Exception as e:
        raise e


def execPowerShell(params):
    if 'cmd' not in params.keys() or params['cmd'] is None or params['cmd'] == '':
        raise Exception('缺少参数')
    try:
        cmd = params["cmd"]
        PowerShellCommand = 'powershell -Command "' + str(cmd) + '"'
        output = os.popen(PowerShellCommand, 'r')
        return output.read()
    except Exception as e:
        raise e


def getSystemPath(params):
    if 'type' not in params.keys() or params['type'] is None or params['type'] == '':
        raise Exception('缺少参数')
    try:
        path = params['type']
        if path == "desktop":
            return os.path.join(GetUserPath(params), "Desktop")
        elif path == "start":
            objShell = win32com.client.Dispatch("WScript.Shell")
            return objShell.SpecialFolders("StartMenu")
        elif path == "install":
            return os.environ['programfiles']
        elif path == "windows":
            return win32api.GetWindowsDirectory()
        elif path == "system":
            return win32api.GetSystemDirectory()
    except Exception as e:
        raise e


def getTempPath(params):
    try:
        return tempfile.gettempdir()
    except Exception as e:
        raise e


def GetUserPath(params):
    try:
        return expanduser("~")
    except Exception as e:
        raise e


if __name__ == '__main__':
    pass

