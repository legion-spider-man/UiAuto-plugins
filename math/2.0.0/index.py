import math


def fabs(params):
    try:
        data = math.fabs(params['num_f'])
        return data
    except Exception as e:
        raise e


def sqrt(params):
    try:
        if params['num_s'] < 0:
            raise Exception("输入的数小于0")
        else:
            return math.sqrt(params['num_s'])
    except Exception as e:
        raise e


def floor(params):
    try:
        return math.floor(params['num_fl'])
    except Exception as e:
        raise e


def round_data(params):
    from decimal import Decimal
    try:
        num = str(params['num_r'])
        n = params['n']
        if n != '' and n != 0:
            data = round(float(num), n)
            print("有小数", data)
        elif n == 0:
            data = int(round(float(num)))
            print("没小数", data)
        return data
    except Exception as e:
        raise e


def factorial(params):
    try:
        return math.factorial(params['num'])
    except ValueError:
        raise Exception("输入的数字不正确")
    except Exception as e:
        raise e


def log(params):
    try:
        return math.log(params['num'])
    except ValueError:
        raise Exception("输入的数字不正确")
    except Exception as e:
        raise e


def cos(params):
    try:
        return math.cos(params['num'])
    except Exception as e:
        raise e


def sin(params):
    try:
        return math.sin(params['num'])
    except Exception as e:
        raise e


def tan(params):
    try:
        return math.tan(params['num'])
    except Exception as e:
        raise e


def atan(params):
    try:
        return math.atan(params['num'])
    except Exception as e:
        raise e


def exp(params):
    try:
        return math.exp(params['num'])
    except Exception as e:
        raise e


if __name__ == '__main__':
    a = find({"string": "aaasdsadasdasdsad12313gh", "regex": "", "p": ""})
    print(a)
