from time import localtime, time, strptime, mktime
from datetime import datetime, timedelta


def get_time(params):
    try:
        time_str = time()
        return time_str
    except Exception as e:
        raise e


def get_year(params):
    try:
        year = localtime(get_time(params)).tm_year
        return year
    except Exception as e:
        raise e


def get_month(params):
    try:
        month = localtime(get_time(params)).tm_mon
        return month
    except Exception as e:
        raise e


def get_day(params):
    try:
        day = localtime(get_time(params)).tm_mday
        return day
    except Exception as e:
        raise e


def get_hour(params):
    try:
        hour = localtime(get_time(params)).tm_hour
        return hour
    except Exception as e:
        raise e


def get_minute(params):
    try:
        minute = localtime(get_time(params)).tm_min
        return minute
    except Exception as e:
        raise e


def get_second(params):
    try:
        second = localtime(get_time(params)).tm_sec
        return second
    except Exception as e:
        raise e


def get_weekday(params):
    try:
        weekday = localtime(get_time(params)).tm_wday
        return weekday + 1
    except Exception as e:
        raise e


def get_week(params):
    try:
        num_day = ['一', '二', '三', '四', '五', '六', '日']
        week = num_day[get_weekday(params) - 1]
        return '星期' + week
    except Exception as e:
        raise e


def get_some(params):
    try:
        if 'year' == str(params['str']):
            data = get_year(params)
        elif 'month' == str(params['str']):
            data = get_month(params)
        elif 'day' == str(params['str']):
            data = get_day(params)
        elif 'hour' == str(params['str']):
            data = get_hour(params)
        elif 'minute' == str(params['str']):
            data = get_minute(params)
        elif 'second' == str(params['str']):
            data = get_second(params)
        elif 'week' == str(params['str']):
            data = get_week(params)

        return data
    except Exception as e:
        raise e


def cal_time(params):
    try:
        date1 = str(params['date1'])
        date2 = str(params['date2'])

        timeArray1 = strptime(date1, "%Y-%m-%d %H:%M:%S")
        timeArray2 = strptime(date2, "%Y-%m-%d %H:%M:%S")

        timestamp1 = mktime(timeArray1)
        timestamp2 = mktime(timeArray2)

        caltime = int(timestamp2 - timestamp1)

        if 'year' == str(params['str']):
            data = caltime / 60 / 60 / 24 / 365
        elif 'month' == str(params['str']):
            data = caltime / 60 / 60 / 24 / 30
        elif 'quarter' == str(params['str']):
            data = caltime / 60 / 60 / 24 / 30 / 3
        elif 'day' == str(params['str']):
            data = caltime / 60 / 60 / 24
        elif 'week' == str(params['str']):
            data = caltime / 60 / 60 / 24 / 7
        elif 'hour' == str(params['str']):
            data = caltime / 60 / 60
        elif 'minute' == str(params['str']):
            data = caltime / 60
        elif 'second' == str(params['str']):
            data = caltime
        if data < 1 and data > 0:
            data = 1
        if data == 0.0:
            data = 0
        return int(data)
    except Exception as e:
        raise e


def addTime(params):
    try:
        timeStr = str(params['time'])
        num = params['num']
        unit = params['unit']
        addStr = str(params['add'])
        addDic = demjson.decode(addStr)
        if num > 0:
            if unit in addDic.keys():
                addDic[unit] = addDic[unit] + num
            else:
                addDic[unit] = num
        # addDic = json.loads(addStr)
        time = datetime.strptime(timeStr, "%Y-%m-%d %H:%M:%S")
        for key, value in addDic.items():
            if key == 'years' or key == "y":
                time = timedelta(days=365 * value) + time
            if key == 'quarters' or key == "Q":
                time = timedelta(days=90 * value) + time
            if key == 'months' or key == "M":
                time = timedelta(days=30 * value) + time
            if key == 'weeks' or key == "w":
                time = timedelta(weeks=1 * value) + time
            if key == 'days' or key == "d":
                time = timedelta(days=1 * value) + time
            if key == 'hours' or key == "h":
                time = timedelta(hours=1 * value) + time
            if key == 'minutes' or key == "m":
                time = timedelta(minutes=1 * value) + time
            if key == 'seconds' or key == "s":
                time = timedelta(seconds=1 * value) + time
            if key == 'milliseconds' or key == "ms":
                time = timedelta(milliseconds=1 * value) + time
        return time.strftime("%Y-%m-%d %H:%M:%S")
    except ValueError as e:
        return e.args[0]
    except Exception as e:
        raise e


def subTime(params):
    try:
        timeStr = str(params['time'])
        num = params['num']
        unit = params['unit']
        addDic = dict()
        if str(params["sub"]).replace(" ", "").replace("\n", "") != "{}":
            addStr = str(params['sub'])
            addDic = demjson.decode(addStr)
        if num > 0:
            if addDic is not None and unit in addDic.keys():
                addDic[unit] = addDic[unit] + num
            else:
                addDic[unit] = num
        # addDic = json.loads(addStr)
        time = datetime.strptime(timeStr, "%Y-%m-%d %H:%M:%S")
        for key, value in addDic.items():
            if key == 'years' or key == "y":
                time = time - timedelta(days=365 * value)
            if key == 'quarters' or key == "Q":
                time = time - timedelta(days=90 * value)
            if key == 'months' or key == "M":
                time = time - timedelta(days=30 * value)
            if key == 'weeks' or key == "w":
                time = time - timedelta(weeks=1 * value)
            if key == 'days' or key == "d":
                time = time - timedelta(days=1 * value)
            if key == 'hours' or key == "h":
                time = time - timedelta(hours=1 * value)
            if key == 'minutes' or key == "m":
                time = time - timedelta(minutes=1 * value)
            if key == 'seconds' or key == "s":
                time = time - timedelta(seconds=1 * value)
            if key == 'milliseconds' or key == "ms":
                time = time - timedelta(milliseconds=1 * value)
        return time.strftime("%Y-%m-%d %H:%M:%S")
    except ValueError as e:
        return e.args[0]
    except Exception as e:
        raise e
