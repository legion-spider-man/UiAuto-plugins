import sys
import time
import pyautogui
import math


def mouse_roll(dict):
    '''
    :param dict:
        方向: direction(up,down),
        动作总时长(秒): times -> float(>0),
        移动像素: px -> int(>0)
    :return: str
    '''

    dict['px'] = int(dict['px'])
    dict['times'] = float(dict['times'])

    if dict['px'] < 50:
        unit_px = 10

    elif dict['px'] >= 1000:
        unit_px = 200
    else:
        unit_px = 50

    if dict['direction'] == 'up':
        px = unit_px
    elif dict['direction'] == 'down':
        px = - unit_px
    else:
        raise Exception('参数错误: direction 参数应为 up 或 down ')

    try:
        if dict['px'] > 0:
            item_count = math.ceil(dict['px'] / unit_px)  # 向上取整
            unit_times = dict['times'] / item_count
            last_time_px = unit_px - (item_count * unit_px - dict['px'])
            sleep_time = unit_times - 0.1
            if sleep_time < 0:
                sleep_time = 0
            print(item_count, unit_times, last_time_px, sleep_time)
            count = 0
            for i in range(0, item_count):
                count += 1
                print("count:", count)
                if count == item_count:
                    if last_time_px == 0:
                        pyautogui.scroll(unit_px)
                    else:
                        pyautogui.scroll(last_time_px)
                        print("最后滚动：", last_time_px, "px")
                else:
                    pyautogui.scroll(px)
                time.sleep(sleep_time)
                print("滚了一下")
            print("需要滚动", item_count, "次", "每次暂停", sleep_time, "秒")
            return '鼠标滚轮{}了{}个像素, 设置时间为{}秒'.format(dict['direction'], dict['px'], dict['times'])
        else:
            raise Exception('参数错误: times参数需遵循 times -> float(>0), px -> int(>0)')
    except Exception as err:
        # return 'Param Error: times参数需遵循 times -> float(>0), px -> int(>0)\n{}'.format(err)
        raise err


def console_result(verify_code):
    sys.stdout.write('<uiauto:result>{}</uiauto:result>'.format(verify_code))


def main(params):
    try:
        print("begin", params)
        verify_code = mouse_roll(params)
    except:
        verify_code = 'ERROR'
    console_result(verify_code)
    return verify_code
